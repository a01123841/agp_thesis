import numpy as np
import os


def correct_zeros_C(group):

    """
    Add C=1 in the minimum time.

    :param group:
    :return:
    """
    index1 = abs(group.t).idxmin()
    group.loc[index1, 'C'] = 1

    return group


def insert_row(group, index1, index2, time_name='TIME', y_name='Y', dose_name='AMT', off='.', on=1):
    """
    Insert a row in the end... Do not remember whyh=


    :param group:
    :param index1:
    :param index2:
    :param time_name:
    :param y_name:
    :param dose_name:
    :param off:
    :param on:
    :return:
    """
    time1 = group.loc[index1, time_name]
    time2 = group.loc[index2, time_name]
    av_time = (time1 + time2) / 2

    # Last row
    row = group.loc[index2, :]
    row[time_name] = av_time
    row[y_name] = off
    row[dose_name] = on
    #     print(row.values)
    group.loc[index1 + 1, :] = row.values
    #     print(group)
    group = group.sort_index()

    return group


def correct_treatment_column(group, input_nan, dose_name='AMT', off='.', on=1):
    """
    This function used to fix the treatment column. It was not working for multitreat.


    :param group:
    :param input_nan:
    :param dose_name:
    :param off:
    :param on:
    :return:
    """

    # Get all indices
    all_index = group.loc[group.AMT == 0].index.values
    # second last
    index1 = all_index[-2]
    # Last
    index2 = all_index[-1]

    # Make all treatment column empty
    group[dose_name] = off
    if input_nan:
        # insert row in index2
        group = insert_row(group, index1, index2, dose_name='AMT', off='.', on=1)
    else:
        group.loc[index2, dose_name] = on
    return group


def shift_times(group, time_name = 'TIME'):
    time = group.TIME.values[0]
    group[time_name] = group[time_name] + abs(time)
    return group


# def nan_when_treat(samples):
#     samples.loc[samples.AMT == 1, 'Y'] = '.'
#     return samples


def postprocess_samples_for_monolix(samples, N_patients=100, columns=['ID', 't', 'MTD', 'C'],
                                    column_names=['ID', 'TIME', 'Y', 'AMT'],
                                    save_folder='/home/galethog/LRZ_Sync+Share/Thesis/SAEM_andres/SAEM_andres/data/tumor',
                                    save_fn='processed_monolix',
                                    save=True,
                                    positive_t=False,
                                    input_nan=False,
                                    correct_treat_col=False,
                                    off = '.',
                                    IDs = None
                                    ):
    print('Processing samples for monolix.')

    time_name = column_names[1]
    dose_name = column_names[3]

    # Remove base value
    samples = samples.loc[samples.ID != 0, :]

    # Select columns
    samples = samples.loc[:, columns]
    # change column names
    dictionary = dict(zip(columns, column_names))
    samples.rename(columns=dictionary, inplace=True)
    # Select N patients
    if N_patients is not None:
        IDs = samples.ID.unique()[0:N_patients]
        samples = samples.loc[samples.ID.isin(IDs)]
    # Convert IDs to int
    samples['ID'] = [np.int(x) for x in samples['ID'].values]

    if correct_treat_col:
        samples = samples.groupby('ID').apply(lambda x: correct_treatment_column(x, input_nan, off=off))
    else:
        # For usual case: all 0s are transformed into .
        samples.loc[samples[dose_name] == 0, dose_name] = off
        samples.loc[samples[dose_name] == 1, dose_name] = 1

    if input_nan:
        samples.reset_index(level=0, drop=True, inplace=True)

    if positive_t:
        samples = samples.groupby('ID').apply(lambda x: shift_times(x))

    if save:
        print('Saving in: ', os.path.join(save_folder, save_fn))
        samples.to_csv(os.path.join(save_folder, save_fn), sep=' ', index=False)
    return samples
