from sklearn.preprocessing import MinMaxScaler
import numpy as np


def minmax_scaler_3d(data):
    scaler = MinMaxScaler(copy=True, feature_range=(0, 1))

    data_copy = data
    n_features = data_copy.shape[2]
    print('Number of features:', n_features)
    data_copy = data_copy.swapaxes(1, 2)

    # list_of_scalers = []
    list_of_features = []
    transformed_data = []

    for jj in np.arange(n_features):
        feature = []
        for i, person in enumerate(data_copy):
            feature.append(person[jj])
        scaler.fit(feature)
        # list_of_scalers.append(scaler)
        list_of_features.append(feature)

        transformed_feature = scaler.transform(feature)
        transformed_data.append(transformed_feature)

    # Assessing:
    list_of_features = np.array(list_of_features)
    list_of_features = list_of_features.swapaxes(0, 1)
    list_of_features = list_of_features.swapaxes(1, 2)

    # if all(list_of_features == data):
    #     print('MinMax Scaling done correctly.')

    transformed_data = np.array(transformed_data)
    transformed_data = transformed_data.swapaxes(0, 1)
    transformed_data = transformed_data.swapaxes(1, 2)

    print('Shape of transformed data:', transformed_data.shape)

    return transformed_data
