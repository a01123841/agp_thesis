import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.integrate import odeint

from .sample_rand_parameter_logn import sample_rand_parameter_logn
from .constants import *

#plt.style.use('ggplot')
from scipy.stats import truncnorm


# ## ###########    PCV PARAMETERS     #################################
#
# # MEAN VALUES (parameter estiamtes):
# P0_pcv_m =       7.13  # [mm]
# Q0_pcv_m =       41.2  # [mm]
# lambda_p_pcv_m = 0.121 # [mo^-1]
# k_PQ_pcv_m =     0.0295 # [mo^-1]
# k_Qp_P_pcv_m =   0.0031 # [mo^-1]
# delta_Qp_pcv_m = 0.00867 # [mo^-1]
# gamma_pcv_m =    0.729  #[]
# KDE_pcv_m =      0.24 # [mo^-1]
#
# # ERROR VALUES of mean distributions (parenthersis in paper):
# P0_pcv_me =          25  # [mm]
# Q0_pcv_me =          7  # [mm]
# lambda_p_pcv_me =    16 # [mo^-1]
# k_PQ_pcv_me =        21 # [mo^-1]
# k_Qp_P_pcv_me =      35 # [mo^-1]
# delta_Qp_pcv_me =    21 # [mo^-1]
# gamma_pcv_me =       37  #[]
# KDE_pcv_me =         33 # [mo^-1]
#
# # INTERINDIVIDUAL VARIABILITY (CV = ration of sigma/mu:
# P0_pcv_cv =          94  # [mm]
# Q0_pcv_cv =          54  # [mm]
# lambda_p_pcv_cv =    72 # [mo^-1]
# k_PQ_pcv_cv =        76 # [mo^-1]
# k_Qp_P_pcv_cv =      97 # [mo^-1]
# delta_Qp_pcv_cv =    75 # [mo^-1]
# gamma_pcv_cv =       115  #[]
# KDE_pcv_cv =         70 # [mo^-1]
#
# # INTERINDIVIDUAL VARIABILITY of mean distributions (parenthersis in paper):
# P0_pcv_cve =          23  # [mm]
# Q0_pcv_cve =          10  # [mm]
# lambda_p_pcv_cve =    9 # [mo^-1]
# k_PQ_pcv_cve =        12 # [mo^-1]
# k_Qp_P_pcv_cve =      91 # [mo^-1]
# delta_Qp_pcv_cve =    12 # [mo^-1]
# gamma_pcv_cve =       9  #[]
# #     KDE_pcv_cve =          # [mo^-1]


def multitreatment(f2, snippets, snippets_shifted,
                   ID, MTD_growth, P_pre, Q_pre,
                   Q_p_pre):
    #     print(MTD_growth)
    #     print(snippets_shifted)
    t_post = []
    for j, t_treat in enumerate(snippets_shifted):
        #     t_treat = np.arange(t_treat, tf, del_t)  # time grid
        y_treat = [P_pre[-1], Q_pre[-1], Q_p_pre[-1]]

        # solve the DEs for pretreatment
        soln_treat = odeint(f2, y_treat, t_treat)
        P_treat = soln_treat[:, 0]
        Q_treat = soln_treat[:, 1]
        Q_p_treat = soln_treat[:, 2]

        MTD_treat = P_treat + Q_treat + Q_p_treat

        # RETURNS:
        P_pre = np.append(P_pre, P_treat[1:])
        Q_pre = np.append(Q_pre, Q_treat[1:])
        Q_p_pre = np.append(Q_p_pre, Q_p_treat[1:])

        MTD_growth = np.append(MTD_growth, MTD_treat[1:])
        #         print(MTD_growth)

        t_post = np.append(t_post, snippets[j][1:])

    return MTD_growth, P_pre, Q_pre, Q_p_pre, t_post


def obtain_snippets(time_dose, delta_t=None):
    time_grid = time_dose.t.values
    t_post = time_grid[time_grid >= 0]

    times = time_dose.loc[time_dose.C == 1, 't']
    last_time = time_dose.tail(1)
    times = times.append(last_time.t)
    #     print(times)
    lower_ts = times.values[:-1]
    upper_ts = times.values[1:]
    #     print(np.arange(len(lower_ts)))

    snippets = []
    snippets_shifted = []
    if delta_t is None:
        for i in np.arange(len(lower_ts)):
            snippet = t_post[np.logical_and(t_post >= lower_ts[i], t_post <= upper_ts[i])]
            snippet_shift = snippet - min(snippet)
            snippets.append(snippet)
            snippets_shifted.append(snippet_shift)
    else:
        for i in np.arange(len(lower_ts)):
            snippet = np.arange(lower_ts[i], upper_ts[i], delta_t)
            try:
                snippet_shift = snippet - min(snippet)
                snippets.append(snippet)
                snippets_shifted.append(snippet_shift)
            except:
                pass
    return snippets, snippets_shifted


def _sample_random_parameters(parameters, treatment, baseline, ID, num=1):
    """ Samples Ribba's parameters if needed.


    Args:
        parameters:
        treatment (bool): With or without treatment.
        baseline:
        ID:
        num:

    Returns:

    """
    if baseline:
        P0, Q0, lambda_p, k_PQ, k_Qp_P, delta_Qp, gamma, KDE = \
            P0_pcv_m, Q0_pcv_m, lambda_p_pcv_m, k_PQ_pcv_m, k_Qp_P_pcv_m, delta_Qp_pcv_m, gamma_pcv_m, KDE_pcv_m
    else:
        if parameters is None:
            # Parameter generation
            P0 = sample_rand_parameter_logn(P0_pcv_m, P0_pcv_cv, num)
            Q0 = sample_rand_parameter_logn(Q0_pcv_m, Q0_pcv_cv, num)
            # Tumor-related parameters:
            lambda_p = sample_rand_parameter_logn(lambda_p_pcv_m, lambda_p_pcv_cv, num)
            k_PQ = sample_rand_parameter_logn(k_PQ_pcv_m, k_PQ_pcv_cv, num)
            # Treatment-related parameters:
            if treatment:
                k_Qp_P = sample_rand_parameter_logn(k_Qp_P_pcv_m, k_Qp_P_pcv_cv, num)
                delta_Qp = sample_rand_parameter_logn(delta_Qp_pcv_m, delta_Qp_pcv_cv, num)
                gamma = sample_rand_parameter_logn(gamma_pcv_m, gamma_pcv_cv, num)
                KDE = sample_rand_parameter_logn(KDE_pcv_m, KDE_pcv_cv, num)
        else:
            ID, P0, Q0, lambda_p, k_PQ = parameters.loc[:, ['ID', 'P0', 'Q0', 'lambda_p', 'k_PQ']].values[0]
            if treatment:
                k_Qp_P, delta_Qp, gamma, KDE = parameters.loc[:, ['k_Qp_P', 'delta_Qp', 'gamma', 'KDE']].values[0]
        if not treatment:
            k_Qp_P = 0
            delta_Qp = 0
            gamma = 0
            KDE = 0
    parameters = np.array([ID, P0, Q0, lambda_p, k_PQ, k_Qp_P, delta_Qp, gamma, KDE])
    parameters_df = pd.DataFrame(data=parameters,
                                 index=['ID', 'P0', 'Q0', 'lambda_p', 'k_PQ', 'k_Qp_P', 'delta_Qp', 'gamma',
                                        'KDE']).transpose()
    return ID, P0, Q0, lambda_p, k_PQ, k_Qp_P, delta_Qp, gamma, KDE, parameters_df


def ribbas_model_full(t0=-20, tf=20, ID=1, del_t=.05, plot=True, treatment=True, ax=None, baseline=False,
                      parameters=None, time_dose=None, multi_treat=False, n_treats=2):
    """  Generation of a time series following Ribbas model.
    Parameters are also sampled within the function.
    Notation:
    P(t) = P, Q(t) = Q, Q_P(t) = Q_p

    Note: keep name of functions for data_generation descriptive.
    Q: How should I store data? Same as pcv data?
    Q: plot within function? (I think not)

    Args:
        t0 (float): Initial time.
        tf (float): Final time.
        ID (int): Identifier for the generated patient's curve and parameters.
        del_t (float): Delta t used for the numerical solver (not sampling).
        plot (bool): Plot the current curve.
        treatment (bool): whether to generate a curve with or without treatment.
        ax: Axis object of where to plot.
        baseline (bool): If True, generate a curve with the mean values of all parameters.
        parameters: (optional) Parameters to generate the corresponding curve using the Model. If nothing is sent,
        parameters are sampled from log-normal distributions.

    Returns:
        parameters_df: A pandas DataFrame containing 4 or 8 parameters (depending on treatment) to generate a curve.
        curve_df: The curve generated within t0 and tf using the model and the generated parameters.
    """

    # [ID, P, Q, Q_P, MTD, C, t, delta_t]
    # ###
    # plot_figure = True
    # treatment = False
    # t0 = -1*np.random.lognormal(t0_mu, t0_sigma, 1)
    # tf =    np.random.lognormal(tf_mu, tf_sigma, 1)
    # ###

    #     lower = 2/del_t
    #     upper = 7/del_t
    #     mu = dt_m
    #     sigma = 2/del_t
    ## Absence of treatment (simplified):
    K = 100
    f2 = []
    if treatment:
        t_treat = 0
    else:
        t_treat = tf

    # if multi_treat and time_dose is None:
    #         t_pre = np.arange(t0, t_treat, del_t)  # time grid
    #         t_post = np.arange(t_treat, tf, del_t)  # time grid

    #         time = np.append(t_pre, t_post)
    #         C = np.append(np.append(np.zeros(len(t_pre)), [1]), np.zeros(len(t_post) - 1))
    #         while True:
    #             pos_2_treat = len(t_pre) + int(np.random.normal(len(t_post) / 2, 5 / del_t))
    #             if pos_2_treat < len(time):
    #                 break

    #         time_dose = pd.DataFrame(data=time, columns=['t'])
    #         time_dose['C'] = C
    #         time_dose.loc[:,'C'].iloc[pos_2_treat] = 1

    if multi_treat and time_dose is None:
        n_treats = n_treats - 1
        lower = 2 / del_t
        upper = 8 / del_t
        mu = 5 / del_t
        sigma = 2 / del_t

        t_pre = np.arange(t0, t_treat, del_t)  # time grid
        t_post = np.arange(t_treat, tf, del_t)  # time grid

        time = np.append(t_pre, t_post)
        # first treatment
        C = np.append(np.append(np.zeros(len(t_pre)), [1]), np.zeros(len(t_post) - 1))
        time_dose = pd.DataFrame(data=time, columns=['t'])
        time_dose['C'] = C
        last_pos = len(t_pre)
        for i in np.arange(n_treats):
            #             while True:
            # #                 pos_n_treat = len(t_pre) + int(np.random.normal(dt_m, 2 / del_t))
            #                 pos_n_treat = last_pos + int(scipy.stats.truncnorm.rvs((lower-mu)/sigma,(upper-mu)/sigma,loc=mu,scale=sigma,size=1)[0])
            # #                 print(pos_n_treat)
            #                 if pos_n_treat < len(time):
            #                     break
            #                 while True:
            #                 pos_n_treat = len(t_pre) + int(np.random.normal(dt_m, 2 / del_t))
            pos_n_treat = last_pos + int(truncnorm.rvs((lower - mu) / sigma, (upper - mu) / sigma, loc=mu, scale=sigma, size=1)[0])
            #                 print(pos_n_treat)
            if pos_n_treat > len(time_dose):
                break

            try:
                time_dose.loc[:, 'C'].iloc[pos_n_treat] = 1
            except:
                break
            # print(time_dose.loc[:, 't'].iloc[pos_n_treat])
            last_pos = pos_n_treat

    if time_dose is not None:
        t0 = time_dose.t.values[0]
        tf = time_dose.t.values[-1]
        if del_t is None:
            if treatment:
                t_pre = time_dose.loc[time_dose.t <= 0, 't'].values
                t_post = time_dose.loc[time_dose.t >= 0, 't'].values
            else:
                t_pre = time_dose.t.values
        else:
            t_pre = np.arange(t0, t_treat, del_t)
    else:
        t_pre = np.arange(t0, t_treat, del_t)  # time grid

    ID, P0, Q0, lambda_p, k_PQ, k_Qp_P, delta_Qp, gamma, KDE, parameters_df = \
        _sample_random_parameters(parameters, treatment, baseline, ID, num=1)

    # solve the system dy/dt = f(y, t) before treatment
    def f1(y, t):
        P = y[0]
        Q = y[1]
        Q_p = y[2]
        # the model equations
        #             k_Qp_P = 0 # trying, removing k_Qp_P (change Damaged to Repaired)
        f0 = lambda_p * P * (1 - (P + Q + Q_p) / K) + k_Qp_P * Q_p - k_PQ * P
        f1 = k_PQ * P
        f2 = -k_Qp_P * Q_p - delta_Qp * Q_p
        return [f0, f1, f2]

    # solve the system dy/dt = f(y, t) after treatment
    if treatment:
        def f2(y, t):
            P = y[0]
            Q = y[1]
            Q_p = y[2]
            # the model equations
            f0 = lambda_p * P * (1 - (P + Q + Q_p) / K) + k_Qp_P * Q_p - k_PQ * P - gamma * np.exp(-KDE * t) * KDE * P
            f1 = k_PQ * P - gamma * np.exp(-KDE * t) * KDE * Q
            f2 = gamma * np.exp(-KDE * t) * KDE * Q - k_Qp_P * Q_p - delta_Qp * Q_p
            return f0, f1, f2

    # initial conditions
    y0 = [P0, Q0, 0]  # initial condition vector

    # solve the DEs for pretreatment
    soln_pre = odeint(f1, y0, t_pre)
    P_pre = soln_pre[:, 0]
    Q_pre = soln_pre[:, 1]
    Q_p_pre = soln_pre[:, 2]

    MTD_growth = P_pre + Q_pre + Q_p_pre

    # With treatment (C has been substituted in the equations):

    if treatment and not multi_treat:
        t_post = np.arange(t_treat, tf, del_t)  # time grid
        y_treat = [P_pre[-1], Q_pre[-1], Q_p_pre[-1]]

        # solve the DEs for pretreatment
        soln_treat = odeint(f2, y_treat, t_post)
        P_treat = soln_treat[:, 0]
        Q_treat = soln_treat[:, 1]
        Q_p_treat = soln_treat[:, 2]

        MTD_treat = P_treat + Q_treat + Q_p_treat

        # RETURNS:
        ID = ID * np.ones(len(t_pre) + len(t_post))

        P = np.append(P_pre, P_treat)
        Q = np.append(Q_pre, Q_treat)
        Q_P = np.append(Q_p_pre, Q_p_treat)

        MTD = np.append(MTD_growth, MTD_treat)

        # C = np.append(np.zeros(len(t_pre)), np.ones(len(t_treat)))  FIXING!
        C = np.append(np.append(np.zeros(len(t_pre)), 1), np.zeros(len(t_post) - 1))
        t = np.append(t_pre, t_post)
    elif treatment and multi_treat:
        #         print('Multitreatment selected')
        snippets, snippets_shifted = obtain_snippets(time_dose, del_t)
        MTD, P, Q, Q_P, t_post = multitreatment(f2, snippets, snippets_shifted,
                                                ID, MTD_growth, P_pre, Q_pre,
                                                Q_p_pre)
        C = time_dose.C.values
        t = np.append(t_pre, t_post)

        if del_t is None:
            t = time_dose.t.values
        ID = ID * np.ones(len(t))
    # print(t, MTD)
    else:
        # RETURNS:
        ID = ID * np.ones(len(t_pre))

        P = P_pre
        Q = Q_pre
        Q_P = Q_p_pre

        MTD = MTD_growth

        C = np.zeros(len(t_pre))
        t = t_pre

    delta_t = np.diff(t)

    if plot:
        if ax is None:
            fig, ax = plt.subplots(1, 1)
        if baseline:
            ax.plot(t, MTD, color='k', lw=3)
        else:
            ax.plot(t, MTD)

    curve_df = pd.DataFrame(data=[ID, MTD, P, Q, Q_P, C, t, delta_t],
                            index=['ID', 'MTD', 'P', 'Q', 'Q_P', 'C', 't', 'delta_t']).transpose()

    return parameters_df, curve_df

