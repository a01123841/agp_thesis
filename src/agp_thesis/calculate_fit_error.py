from .constants import error_options
from .label_load_save_predictions import label_load_save_predictions
import numpy as np


def fit_error(list_true_samples_test, list_pred_samples_test, error, n_trained):
    error_function = error_options[error]

    list_of_lists_error = []
    for ii, (true_samples, pred_samples) in enumerate(zip(list_true_samples_test, list_pred_samples_test)):
        true_grouped = true_samples.groupby('ID')
        pred_grouped = pred_samples.groupby('ID')

        list_error = []
        list_means_error = []
        for jj, ID in enumerate(list(true_grouped.groups.keys())):
            if n_trained is None:
                n_samples = len(true_grouped.get_group(ID))
                true = true_grouped.get_group(ID).iloc[:n_samples, :]
                pred = pred_grouped.get_group(ID).iloc[:n_samples, :]
            else:
                true = true_grouped.get_group(ID).iloc[:n_trained, :]
                pred = pred_grouped.get_group(ID).iloc[:n_trained, :]
            error = error_function(true.MTD.values, pred.MTD.values)
            list_error.append(error)  # 3000 values
            mean_error = np.mean(list_error)  # mean of 3000 values

        print(error, ' is : ', mean_error)
        print('RMSE', np.sqrt(mean_error), '[mm]')

        list_means_error.append(mean_error)
        list_of_lists_error.append(list_error)

    return list_of_lists_error, list_means_error


def calculate_fit_error(generation_kw, generation_opts, plot_opts,
                        n_trained=17,
                        error='mape',
                        noise_sigma=[0], activation=['prelu'],
                        cv_key=None, it=0.0, gpu=False, plot=False,
                        pool_folder='../Data/Pool', model_folder='../Data/New_Folder',
                        pred_folder='../Data/Predicted',
                        d_np=10, N_plot=5, max_dn=30, return_pred_curves=False,
                        test_IDs=None, N_test=None,
                        y_test=None,
                        y_prime=None,
                        true_samples_test=None,
                        plot_noised=False, **others):

    """

    Args:
        generation_kw:
        generation_opts:
        plot_opts:
        n_trained:
        error:
        noise_sigma:
        activation:
        cv_key:
        it:
        gpu:
        plot:
        pool_folder:
        model_folder:
        pred_folder:
        d_np:
        N_plot:
        max_dn:
        return_pred_curves:
        test_IDs:
        N_test:
        y_test:
        y_prime:
        true_samples_test:
        plot_noised:
        **others:

    Returns:

    """

    list_y_test, list_y_prime, list_true_samples_test, list_pred_samples_test, list_pred_curves_test, test_IDs = \
        label_load_save_predictions(generation_kw, generation_opts, plot_opts,
                                    noise_sigma=noise_sigma, activation=activation,
                                    cv_key=cv_key, it=it, gpu=gpu, plot=plot,
                                    pool_folder=pool_folder,
                                    model_folder=model_folder, pred_folder=pred_folder,
                                    d_np=d_np, N_plot=N_plot, max_dn=max_dn,
                                    return_pred_curves=return_pred_curves,
                                    plot_noised=plot_noised,
                                    test_IDs=test_IDs, N_test=N_test,
                                    y_test=y_test,
                                    y_prime=y_prime,
                                    true_samples_test=true_samples_test)
#     return list_true_samples_test, list_pred_samples_test
    print('FINISHED LOADING ALL TRUE/PRED PAIRS')

    list_of_lists_error, list_means_error = fit_error(list_true_samples_test, list_pred_samples_test, error, n_trained)

    return list_y_test, list_y_prime, list_true_samples_test, list_pred_samples_test, list_pred_curves_test, test_IDs, \
           list_of_lists_error, list_means_error
