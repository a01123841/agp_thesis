import pandas as pd
import numpy as np
# from .mspe_loss import mspe_loss
# from .mean_squared_error import mean_squared_error
# from .mean_absolute_percentage_error import mean_absolute_percentage_error
# from .root_mean_squared_error import root_mean_squared_error
# from .pearson_chi_square import pearson_chi_square
# from keras.layers import ELU
# from keras.layers.advanced_activations import LeakyReLU, PReLU, ThresholdedReLU
#
#

def underlying_normal(mu, sigma):
    under_m = 2 * np.log(mu) - 1 / 2 * np.log(sigma ** 2 + mu ** 2)
    under_s = np.sqrt(-2 * np.log(mu) + np.log(sigma ** 2 + mu ** 2))
    return under_m, under_s


theta_i = 0
theta_f = 500

Kp_i = .5
Kp_f = 1.5

Tp_i = 10
Tp_f = 200

# MEAN VALUES of PARAMETERS

Tp_m, Kp_m, theta_m = np.mean([Tp_i, Tp_f]), np.mean([Kp_i, Kp_f]), np.mean([theta_i, theta_f])
dt_m_sam =      10 # [s]
n_p_m_sam =     50 # Gaussian


# MU VALUES (LOGNORMAL PARAMETERS):
t0_mu_sam =      100  # [s]
tf_mu_sam =      500  # [s]

# ERROR VALUES (parenthersis in paper):
t0_sigma_sam =      2   # [s]
tf_sigma_sam =      0.6 # [s]

t0_under_m_sam, t0_under_s_sam = underlying_normal(t0_mu_sam, t0_sigma_sam)
tf_under_m_sam, tf_under_s_sam = underlying_normal(tf_mu_sam, tf_sigma_sam)