import pandas as pd
from .error_cubes import error_cubes


def general_cv_errors(generation_kw, cv_options, noise_sigma=[0], activations=['linear'],
                      errors=['mse', 'mape', 'rmse'], unique_id='', save_df=True, it=0,
                      gpu=False, folder_path='New_Folder', key_as_uniqueid=False):

    cumulative_mse = pd.DataFrame()
    cumulative_mape = pd.DataFrame()
    cumulative_rmse = pd.DataFrame()

    for cv_key, value in cv_options.items():
        if key_as_uniqueid:
            list_of_cubes = error_cubes(generation_kw, noise_sigma=noise_sigma, activations=activations, errors=errors,
                    unique_id=cv_key, save_df=save_df, it=it, gpu=gpu, folder=folder_path, cv_key=cv_key)
        else:
            list_of_cubes = error_cubes(generation_kw, noise_sigma=noise_sigma, activations=activations, errors=errors,
                    unique_id=unique_id, save_df=save_df, it=it, gpu=gpu, folder=folder_path, cv_key=cv_key)

        cube_mse = list_of_cubes[0]
        cube_mape = list_of_cubes[1]
        cube_rmse = list_of_cubes[2]

        cube_mse.index.set_levels([cv_key], level=0, inplace=True)
        cube_mape.index.set_levels([cv_key], level=0, inplace=True)
        cube_rmse.index.set_levels([cv_key], level=0, inplace=True)

        cumulative_mse = cumulative_mse.append(cube_mse)
        cumulative_mape = cumulative_mape.append(cube_mape)
        cumulative_rmse = cumulative_rmse.append(cube_rmse)

    return cumulative_mse, cumulative_mape, cumulative_rmse
