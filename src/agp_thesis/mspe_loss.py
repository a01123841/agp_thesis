from keras import backend as K


def mspe_loss(y_true,y_pred):

    return K.mean(K.square(y_true - y_pred)/K.square(y_true))