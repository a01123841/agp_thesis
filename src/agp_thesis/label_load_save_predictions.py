from .create_id import create_id
from .plot_utils import group_and_scatter
from .generate_predictions import generate_predictions
import pandas as pd
import os


def plot_predictions(true_samples_test, pred_samples_test, test_IDs, N_plot, treatment, plot_baseline, d_np,
                     true_extension, color=None, linestyle='-', ax=None, plot_samples=True,
                     title='MTD vs Time, sampling', label=None):

    # print('ax', ax)
    samples_to_plot = true_samples_test.loc[true_samples_test['ID'].isin(test_IDs.values[:N_plot])]
    predictions_to_plot = pred_samples_test.loc[
        pred_samples_test['ID'].isin(test_IDs.values[:N_plot])]

    # Get here the times of the treatments
    # Take  axis and plot vertical lines
    # Change function with return_ax = True and change returns to give axis.
    # also take an axis as argument so it can plot the SAEM on top
    # put the respective saem code in the for loop
    # add labels "LSTM", "SAEM"

    # Plot real data
    if plot_samples:
        # _, ax = group_and_scatter(samples_to_plot, x_label='t', y_label='MTD',
        #                           title=title,
        #                           N=N_plot, scatter=True, ax=ax, treatment=treatment,
        #                           plot_baseline=plot_baseline,
        #                           return_ax=True, color='k', label='')
        _, ax = group_and_scatter(samples_to_plot, x_label='t', y_label='MTD',
                                  title=title,
                                  N=N_plot, scatter=True, ax=ax, treatment=treatment,
                                  plot_baseline=plot_baseline,
                                  return_ax=True, color='k', label='Samples')
        minx = min(true_samples_test.t.values)
        maxx = max(pred_samples_test.t.values)
        miny = 0
        maxy = 120
        ax.set_xlim([minx, maxx])
        ax.set_ylim([miny, maxy])

        if true_extension is not None:
            minx = min(true_samples_test.t.values)
            maxx = max(true_extension.t.values)
            ax.set_xlim([minx, maxx])
            _, ax = group_and_scatter(true_extension, x_label='t', y_label='MTD',
                                      N=N_plot, scatter=True, ax=ax, treatment=treatment,
                                      plot_baseline=plot_baseline,
                                      return_ax=True, facecolors=None, marker='x', color='k')
    # Plot fit curve
    _, ax = group_and_scatter(predictions_to_plot, x_label='t', y_label='MTD', title=title,
                              N=N_plot, scatter=False, ax=ax, treatment=treatment,
                              plot_baseline=False, return_ax=True, alpha=1, lw=2, color=color, linestyle=linestyle,
                              label=label)
    # # plot prediction for future times
    # _ = group_and_scatter(predictions_to_plot, x_label='t', y_label='MTD',
    #                       title='MTD vs Time, sampling',
    #                       N=N_plot, scatter=False, ax=ax, treatment=treatment,
    #                       plot_baseline=False, alpha=.2, n_p=d_np, color='black', lw=1)
    # print('plot_samples', plot_samples)
    return ax


def _prepare_pool_filenames(pool_folder, identifier, plot_noised):
    fname_parameters = os.path.join(pool_folder, 'art_parameters' + identifier)
    if plot_noised:
        fname_samples = os.path.join(pool_folder, 'art_samples_noised' + identifier)
        # identifier_pred = identifier + '_noised'
    else:
        fname_samples = os.path.join(pool_folder, 'art_samples' + identifier)
    identifier_pred = identifier
    fname_curves = os.path.join(pool_folder, 'art_curves' + identifier)

    return fname_parameters, fname_samples, fname_curves, identifier_pred


def _prepare_pred_file_names(key, activation, identifier, identifier_pred, pred_folder):
    if key is None or key is '':
        ytrue_name = 'y_true' + '_' + activation + identifier
        yprime_name = 'y_prime' + '_' + activation + identifier
        fname_samples_pred = os.path.join(pred_folder,
                                          'pred_samples' + '_' + activation + identifier_pred)
        fname_curves_pred = os.path.join(pred_folder,
                                         'pred_curves' + '_' + activation + identifier_pred)

    else:
        ytrue_name = 'y_true' + '_' + activation + '_' + key + identifier
        yprime_name = 'y_prime' + '_' + activation + '_' + key + identifier
        fname_samples_pred = os.path.join(pred_folder,
                                          'pred_samples' + '_' + activation + '_' + key + identifier_pred)
        fname_curves_pred = os.path.join(pred_folder,
                                         'pred_curves' + '_' + activation + '_' + key + identifier_pred)

    return ytrue_name, yprime_name, fname_samples_pred, fname_curves_pred


def _recover_X_test(true_samples, y_test, y_prime, pool_folder, model_folder, identifier, test_IDs=None):
    if test_IDs is None:
        try:
            test_IDs = pd.read_pickle(os.path.join(pool_folder, 'test_IDs' + '_' + identifier))
            #         true_samples_test = true_samples.loc[true_samples['ID'].isin(y_test.ID.values)]
        except:
            # train_IDs = pd.read_pickle(os.path.join(pool_folder, 'train_IDs' + '_' + identifier))
            try:
                test_IDs = pd.read_pickle(os.path.join(pool_folder, 'test_IDs' + '_' + identifier))
            except:
                test_IDs = pd.read_pickle(os.path.join(model_folder, 'test_IDs' + '_' + identifier))

            print('Number of curves to test: ', test_IDs.shape)

    y_test['ID'] = test_IDs.values
    y_prime['ID'] = test_IDs.values
    true_samples_test = true_samples.loc[true_samples['ID'].isin(y_test.ID.values)]

    return test_IDs, true_samples_test, y_test, y_prime


    # TODO
    # if n_p + d_np file exist, load it, use np for plotting real, use np+dnp for plotting real extender and prediction
    # if it does not exist, create extension and use it for plotting
    # if i want the next 5 points but i have 10 extra points, i should not generate  more.
    # also if i want to train only with 17 but i have 34, i should not generate a new file. when i generate data i
    # should define n_p (which will be the number of points between t1 and t2), then use dt to generate points until 100 are reached.

    # functions to modify: generate_load artificial data..
    # by default, np sent to generate prediction are 100, then we plot only d_np


def label_load_save_predictions(generation_kw, generation_opts, plot_opts,
                                noise_sigma=[0], activations=['prelu'], cv_key=None,
                                it=0, gpu=False, plot=True,
                                pool_folder='../Data/Pool', model_folder='../Data/New_Folder',
                                pred_folder='../Data/Predicted',
                                d_np=None, N_plot=15, max_dn=50,
                                return_pred_curves=True, plot_noised=False,
                                test_IDs=None, N_test=None,
                                y_test=None,
                                y_prime=None,
                                true_samples_test=None,
                                multi_treat=False, fine_sampling=False,
                                extend_original=False,
                                free_n_treat=False,
                                color=None, linestyle='-',
                                ax=None, return_ax=False,
                                plot_samples=True, title=None, label=None,
                                **others):
    """
    Given all parameters that characterize a previously trained model, load y and y' from the test set (called
    validation set during training) to generate the corresponding pair of patients' full curves.
    Important: i test_IDs are not sent, they must be available in the corresponding model_folder.

    If plot is True, N_plots are shown where the points are the sample points (training data)
    and the solid lines are the estimated curves. The solid gray lines are continuation of the curves for a time larger
    than the time used for the samples, representing prediction of the behavior of the tumor.

    It is possible to send a cv_key, noise_sigma, or activations in case the model was trained repeated times when
    performing a Grid Search for a hyperparameter.

    Key function: generate_predictions()

    :param generation_kw: If sent, then the function will look for the validation set in the folder.
    :param generation_opts:
    :param plot_opts:
    :param noise_sigma:
    :param activations:
    :param cv_key:
    :param it:
    :param gpu:
    :param plot:
    :param pool_folder:
    :param model_folder:
    :param pred_folder:
    :param d_np: Only relevant when plotting. Number of points (in total) to plot in prediction.
    :param N_plot: Number of curves to show in plot.
    :param max_dn: Maximum number of points to be added after the last sample from every series.
    :param return_pred_curves:
    :param plot_noised:
    :param others:
    :param multi_treat: boolean. Whether search for more than one treatments.
    :param fine_sampling: False works for easy fit error calculation. True works for better fit, tricky error calc.

    :return:
    """

    if not isinstance(cv_key, list):
        cv_key = [cv_key]

    treatment = generation_kw['treatment']
    plot_baseline = plot_opts['add_base']

    list_true_samples_test = []
    list_true_samples_extension = []
    list_pred_samples_test = []
    list_pred_curves_test = []
    list_y_test = []
    list_y_prime = []

    if N_test is not None:
        test_IDs = test_IDs.iloc[0:N_test]
    for ii, current_sigma in enumerate(noise_sigma):

        identifier = create_id(**generation_kw, current_sigma=current_sigma, it=it, gpu=gpu, add_sigma=True,
                               multi_treat=multi_treat, free_n_treat=free_n_treat)

        fname_parameters, fname_samples, fname_curves, identifier_pred = \
            _prepare_pool_filenames(pool_folder, identifier, plot_noised)

        if not os.path.isfile(fname_samples):
            raise ValueError(
                'Error. Original curves, used for training and testing, do not exist in pool_folder. Name: ',
                fname_samples)
        else:
            if y_prime is not None and true_samples_test is not None:
                print('Using sent data set.')
                test_IDs = true_samples_test.groupby('ID').first().reset_index().ID
                print('Predicted curves do not exist, creating curves with name', identifier_pred)
                if fine_sampling:
                    print('Fine sampling selected.')
                else:
                    print('Fine sampling not selected.')

                A = generate_predictions(true_samples_test, del_t=5,
                                         **generation_kw, **generation_opts,
                                         **plot_opts,
                                         return_pred_curves=return_pred_curves,
                                         parameters=y_prime,
                                         fine_sampling=fine_sampling,
                                         multi_treat=multi_treat,
                                         IDs=test_IDs,
                                         copy_times=True, max_dn=max_dn,
                                         extend_original=extend_original,
                                         y_original=y_test)
                if extend_original:
                    print('Extension of original curves selected.')
                    pred_samples_test, pred_curves_test, true_extension = A
                    # print(true_extension)
                    list_true_samples_extension.append(true_extension)
                else:
                    true_extension = None
                    pred_samples_test, pred_curves_test = A


                list_true_samples_test.append(true_samples_test)
                list_pred_samples_test.append(pred_samples_test)
                list_pred_curves_test.append(pred_curves_test)
                list_y_test.append(y_test)
                list_y_prime.append(y_prime)

                if plot:
                    ax = plot_predictions(true_samples_test, pred_samples_test, test_IDs, N_plot, treatment,
                                          plot_baseline, d_np, true_extension, color=color, linestyle=linestyle,
                                          ax=ax, plot_samples=plot_samples, title=title, label=label)

            elif os.path.isfile(fname_samples):
                print('Found original curves used for training and testing with name', fname_samples)
                true_samples = pd.read_pickle(fname_samples)

                for jj, key in enumerate(cv_key):
                    for kk, activation in enumerate(activations):

                        ytrue_name, yprime_name, fname_samples_pred, fname_curves_pred = \
                            _prepare_pred_file_names(key, activation, identifier, identifier_pred, pred_folder)

                        y_test = pd.read_pickle(os.path.join(model_folder, ytrue_name))
                        y_prime = pd.read_pickle(os.path.join(model_folder, yprime_name))
                        if N_test is not None:
                            y_test = y_test.iloc[0:N_test, :]
                            y_prime = y_prime.iloc[0:N_test, :]
                            print('y_test shape:', y_test.shape)

                        print('Using data set from folder.')
                        test_IDs, true_samples_test, y_test, y_prime = _recover_X_test(true_samples, y_test, y_prime,
                                                                                       pool_folder, model_folder,
                                                                                       identifier,
                                                                                       test_IDs)

                        if os.path.isfile(fname_curves_pred):
                            print('Predicted curves already exist, loading curves with name', identifier_pred)
                            pred_samples_test = pd.read_pickle(fname_samples_pred)
                            pred_curves_test = pd.read_pickle(fname_curves_pred)
                        else:
                            print('Predicted curves do not exist, creating curves with name', identifier_pred)
                            pred_samples_test, pred_curves_test = generate_predictions(true_samples_test, del_t=5,
                                                                                       **generation_kw, **generation_opts,
                                                                                       **plot_opts,
                                                                                       return_pred_curves=return_pred_curves,
                                                                                       parameters=y_prime,
                                                                                       fine_sampling=fine_sampling,
                                                                                       multi_treat=multi_treat,
                                                                                       IDs=test_IDs,
                                                                                       copy_times=True, max_dn=max_dn)
                            pred_samples_test.to_pickle(fname_samples_pred)
                            if return_pred_curves:
                                pred_curves_test.to_pickle(fname_curves_pred)

                        print('Shapes y_test, y_prime, trues_samples_test, test_IDs',
                              [x.shape for x in [y_test, y_prime, true_samples_test, test_IDs]])

                        list_true_samples_test.append(true_samples_test)
                        list_pred_samples_test.append(pred_samples_test)
                        list_pred_curves_test.append(pred_curves_test)
                        list_y_test.append(y_test)
                        list_y_prime.append(y_prime)

                        if plot:
                            ax = plot_predictions(true_samples_test, pred_samples_test, test_IDs, N_plot, treatment,
                                                  plot_baseline, d_np, color=color, linestyle=linestyle,
                                                  ax=ax, plot_samples=plot_samples, title=title, label=label)
    if return_ax:
        return list_y_test, list_y_prime, list_true_samples_test, list_pred_samples_test, list_pred_curves_test, list_true_samples_extension, test_IDs, ax
    else:
        return list_y_test, list_y_prime, list_true_samples_test, list_pred_samples_test, list_pred_curves_test, list_true_samples_extension, test_IDs

