import matplotlib.pyplot as plt
from .ribbas_model_full import ribbas_model_full
from .plot_utils import group_and_scatter
from .constants import *
from tqdm import tqdm

# plt.style.use('ggplot')


def _asserts(dt, n_p, plot_curves, ax, plot_title, parameters, extend_original, y_original):
    if dt is None and n_p is None:
        raise ValueError(
            'Distance between sampling points can be calculated with EITHER dt or n_p. Both cannot be None.')

    if parameters is not None and not isinstance(parameters, pd.DataFrame):
        raise ValueError('parameters should be None or DataFrame.')

    if plot_curves and ax is None:
        fig, ax = plt.subplots()
        ax.set_title(plot_title)
        ax.set_ylim(0, 110)
        ax.set_xlim(-60, 60)
        ax.set_xlabel("t")
        ax.set_ylabel("MTD = P+Q+Q_p")
        ax.axvline(x=0, lw=.5)
    else:
        ax = None
    if extend_original and y_original is None:
        raise ValueError('For extension of original curves, y_original must be sent too.')
    if y_original is not None and not extend_original:
        raise ValueError('y_original was sent but will be disregarded cause extend_original is False...')
    # return fix_t0, fix_tf, ax, n_p, dt
    return ax, n_p, dt


def _get_times(samples_group, fine_sampling, dt, max_dn):
    # Extension of time grid.

    times = samples_group.t
    t1 = min(times)
    ext_0 = max(times) + dt
    ext_times = np.cumsum([dt] * max_dn) + ext_0
    if fine_sampling:
        d_rows = dt / .05
        # ext_times = np.cumsum([dt] * max_dn) + ext_0
        # ext_times = np.arange(ext_0, ext_0 + dt*max_dn,.05)
        # ext_times = np.cumsum([dt] * max_dn) + ext_0
        new_row = max(times.index) + d_rows
        ext_rows = np.cumsum([100] * max_dn) + new_row
        times = times.append(pd.Series(data=ext_times, index=ext_rows))  # Use times.index as rows to sample
        t2 = max(times)
        if t1 > 0:
            raise ValueError('Error, t1 is positive. Times t1, t2', t1, t2)
        dt_ode=.05
        time_dose = samples_group.loc[:, ['t', 'C']]
        C = time_dose.C.values
        time_dose2 = pd.DataFrame(data=[], columns=time_dose.columns)
        time_dose2['t'] = times
        time_dose2['C'] = np.pad(C, (0, len(times) - len(C)), 'constant', constant_values=0)
        # time_dose2['t'] = np.append(times, ext_times)
        # time_dose2['C'] = np.pad(C, (0, len(np.append(times, ext_times)) - len(C)), 'constant', constant_values=0)
    else:
        time_dose = samples_group.loc[:, ['t', 'C']]
        times = np.append(times, ext_times)
        t2 = max(times)
        C = time_dose.C.values
        time_dose2 = pd.DataFrame(data=[], columns=time_dose.columns)
        time_dose2['t'] = times
        time_dose2['C'] = np.pad(C, (0, len(times) - len(C)), 'constant', constant_values=0)
        dt_ode = None
    return t1, t2, times, ext_times, time_dose2, dt_ode



def _sample_from_curve(n_p, max_dn, copy_times, times, CURVES):
    n_total = n_p + max_dn

    if copy_times:
        try:
            rows = times.index
            SAMPLE = CURVES.loc[rows, :].iloc[0:n_total, :]
        except:
            SAMPLE = CURVES.loc[CURVES.t.isin(times), :]

        # SAMPLE = CURVES.loc[CURVES.t.isin(times), :]
        # PRED = CURVES.loc[CURVES.t > max(SAMPLE.t)]
        # SAMPLE = SAMPLE.append(PRED)
        # print(len(CURVES), len(PRED), len(SAMPLE))
    else:
        # sample uniformely
        d_rows = int(np.floor(len(CURVES) / n_total))
        SAMPLE = CURVES.iloc[::d_rows, :][0:n_total]

    return SAMPLE


def generate_predictions(true_curves, n_p=None, del_t=5, treatment=True, return_pred_curves=True,
                         uniform_dt=True,
                         plot_curves=True, plot_samples=True, plot_base=True,
                         add_base=True, plot_title="Curves with predicted parameters",
                         ax=None, parameters=None,
                         fine_sampling=False, multi_treat=False,
                         extend_original=False, y_original=None,
                         IDs=None, copy_times=False, max_dn=100,
                         **others):
    """

    Args:
        :param true_curves: Can be None if parameters and time_dose is sent.
        :param n_p:
        :param del_t:
        :param treatment:
        :param return_pred_curves:
        :param uniform_dt:
        :param plot_curves:
        :param plot_samples:
        :param plot_base:
        :param add_base:
        :param plot_title:
        :param ax:
        :param parameters:
        :param time_dose: If sent, it will build a curve with these times. No sampling done; SAMPLE would be same as CURVE.
        :param IDs:
        :param copy_times:
        :param max_dn: Maximum number of points to be added after the last sample from every series.
        :param **others:

    Returns:

    """

    # TODO: add bound_MTD = False
    #       add bound_param = False
    #       sigma_noise = ____, baseline = True,
    # default dt = 5 cause of the range of time means (-37 to 60)
    #     divided by average number of points per time series (18)
    # dt = 5 means sampling every 100th point
    # If del_t or n_p are None, they will be sampled individually for every curve.
    ## Curves with differents parameters (sampled in-function):
    ax, n_p, del_t = _asserts(del_t, n_p, plot_curves, ax, plot_title, parameters, extend_original, y_original)

    # Initialize DataFrames with the mean values
    mean_parameters, mean_patient = ribbas_model_full(-37, 50, ID=0, ax=ax, baseline=True, plot=False,
                                                      treatment=treatment)
    samples_grouped = true_curves.groupby('ID')
    samples = mean_patient
    curves = []
    extensions = []
    if return_pred_curves:
        curves = mean_patient
    if extend_original:
        extensions = mean_patient
    if IDs is None:
        IDs = parameters['ID']

    for i, ID in tqdm(enumerate(IDs)):
        if not uniform_dt:
            n_p = len(samples_grouped.get_group(ID))

        # # Extension of time grid.
        # if fine_sampling:
        #     time_dose = None
        #     dt=.05
        # else:
        #     dt=None
        #     time_dose = samples_grouped.get_group(ID).loc[:, ['t', 'C']]
        t1, t2, times, t_extension, time_dose, dt = _get_times(samples_grouped.get_group(ID), fine_sampling, del_t, max_dn)

        ind_parameters = parameters.loc[parameters.ID == ID]

        # GENERATE ARTIFICIAL CURVE WITH PARAMETERS:
        if fine_sampling:
            # generate from t1 until tf, with .05 dt, then, take time values of C
            time_dose_pred = pd.DataFrame(data=[], columns=time_dose.columns)
            time_dose_pred['t'] = np.arange(times.values[0], times.values[-1], .05)
            time_dose_pred['C'] = np.zeros(len(time_dose_pred))
            times_of_C = time_dose.loc[time_dose.C == 1, 't'].values
            indices = []
            for t_C in times_of_C:
                indices.append(abs(time_dose_pred.t-t_C).idxmin())
            time_dose_pred.loc[indices, 'C'] = 1
            _, CURVES = ribbas_model_full(t1, t2 + .05, ID=ID, ax=ax, plot=plot_curves, treatment=treatment,
                                          parameters=ind_parameters, time_dose=time_dose_pred, multi_treat=multi_treat,
                                          del_t=None)
            # _, CURVES = ribbas_model_full(t1, t2 + .05, ID=ID, ax=ax, plot=plot_curves, treatment=treatment,
            #                               parameters=ind_parameters, time_dose=None, multi_treat=multi_treat,
            #                               del_t=dt)
            if return_pred_curves:
                curves = pd.concat([curves, CURVES])
            # SAMPLE = _sample_from_curve(n_p, max_dn, copy_times, times, CURVES)
        else:
            _, CURVES = ribbas_model_full(t1, t2 + .05, ID=ID, ax=ax, plot=plot_curves, treatment=treatment,
                                          parameters=ind_parameters, time_dose=time_dose, multi_treat=multi_treat,
                                          del_t=dt)
        SAMPLE = CURVES
        samples = pd.concat([samples, SAMPLE])

        if y_original is not None and extend_original:
            ind_parameters_original = y_original.loc[y_original.ID == ID]
            # print('time_dose', time_dose)
            _, EXTENSION = ribbas_model_full(t1, t2 + .05, ID=ID, ax=ax, plot=plot_curves, treatment=treatment,
                                             parameters=ind_parameters_original, time_dose=time_dose,
                                             multi_treat=True,
                                             del_t=None)
            # print('EXTENSION', EXTENSION)
            EXTENSION = EXTENSION.loc[EXTENSION.t.isin(t_extension), :]
            extensions = pd.concat([extensions, EXTENSION])
    samples = samples.dropna()  # dropna or coerce
    if not add_base:
        samples = samples.loc[samples.ID != 0]

    if plot_curves:
        if plot_base:
            ignore = ribbas_model_full(-37, 50, ID=0, ax=ax, baseline=True, plot=True, treatment=treatment)
        if treatment:
            ax.axvline(x=0, color='k', lw=.5)
        plt.show()

    if plot_samples:
        ignore = group_and_scatter(samples, x_label='t', y_label='MTD', title='MTD vs Time, sampling',
                                   scatter=True, ax=None, treatment=treatment, plot_baseline=plot_base)
    if not return_pred_curves:
        curves = []

    if extend_original:
        extensions = extensions.loc[extensions.ID != 0]
        return samples, curves, extensions
    else:
        return samples, curves


