import matplotlib.pyplot as plt
# #plt.style.use('ggplot')
from .ribbas_model_full import ribbas_model_full
from .samads_model import samads_model
from .constants import *

model_options = {'Ribbas': ribbas_model_full,
                 'Samads': samads_model
                }


def plot_constraint(curve, return_ax=False):
    fig, ax = plt.subplots()
    ax.plot(curve.t, curve.MTD, lw=2, label='MTD')
    ax.plot(curve.t, curve.P, lw=1, label='P', ls='--')
    ax.plot(curve.t, curve.Q, lw=1, label='Q', ls='--')
    try:
        ax.plot(curve.t, curve.Q_P, lw=1, label='Q_P', ls='--')
        ax.axvline(0, alpha=.3)
        ax.legend(labels=['$MTD$', '$P$', '$Q$', '$Q_P$'])
    except:
        ax.legend(labels=['$MTD$', '$P$', '$Q$'])

    ax.set_ylabel('Diameter [mm]')
    ax.set_xlabel('t')
    ax.set_xlim([min(curve.t) - 10, max(curve.t) + 10])
    ax.set_ylim([-10, max(curve.MTD) + 10])

    if return_ax:
        return ax
    else:
        pass


def line_with_confidence_interval(axis, x_values, y_values, color='#023858', label='Plot', x_lims=None, y_lims=None,
                                  plot_error=False, sigma_y=None, alpha=.1, linestyle='-', marker=''):
    """
        Helper function for plotting a line with confidence intervals in a desired set of axis.
    """
    if sigma_y is not None and y_lims is not None:
        sigma_y = [(2 * y_lims[1]) if np.isnan(i) else i for i in sigma_y]
    axis.plot(x_values, y_values, color=color, label=label, linestyle=linestyle, marker=marker)
    axis.legend(framealpha=0.4, loc='upper left')
    if plot_error:
        axis.fill_between(x_values, y_values - sigma_y, y_values + sigma_y, color=color, alpha=alpha)
    axis.set_ylim(y_lims)
    if x_lims is None:
        axis.set_xlim([x_values[0], x_values[-1]])
    else:
        axis.set_xlim(x_lims)


def group_and_scatter(df, by = 'ID', x_label='Time', y_label='MTD', title='MTD vs Time', scatter=False, ax=None,
                      plot_baseline=True, N=None, treatment=True, return_ax=False, alpha=1, color=None, n_p=None,
                      lw=None, model_name='Ribbas', xmin=t0_m, xmax=tf_m, ymin=0, ymax=110, facecolors=None, marker=None,
                      label=None, linestyle='-'):
    """ Group and scatter
    """
    model = model_options[model_name]

    if ax is None:
        fig, ax = plt.subplots()
        ax.set_title(title)
        ax.set_ylim(ymin, ymax)
        ax.set_xlim(xmin, xmax)
        ax.set_xlabel(x_label)
        ax.set_ylabel(y_label)
        # ax.axvline(x=0, lw=.5)

    if N is None:
        N = int(np.max(df.ID))

    df_grouped = df.groupby(by)
    N_keys = list(iter(df_grouped.groups.keys()))[:N]

    if isinstance(color, list):
        if len(color) < N:
            raise ValueError('N and length of colors do not match')
        colors = color
    else:
        colors = None

    if isinstance(label, list):
        if len(label) < N:
            raise ValueError('N and length of labels do not match')
        labels = label
    else:
        labels = None

    # true_samples_test.groupby('ID').apply(lambda x: x.plot(x='t', y='MTD'))
    # list(df_grouped)[:(N+1)]
    # result = [g[1] for g in list(grouped)[:3]]
    for i, key in enumerate(N_keys):
        group = df_grouped.get_group(key)
        if isinstance(colors, list):
            color = colors[i]
        if isinstance(labels, list):
            label = labels[i]

        X = group.loc[:, x_label]
        Y = group.loc[:, y_label]
        if isinstance(n_p, int):
            X = X[:n_p]
            Y = Y.iloc[:n_p]

        if treatment and label is 'plot_treat':
            vertical = group.loc[group.C == 1, x_label].values
            [ax.axvline(x=v, lw=.5) for v in vertical]
        if label is None:
            label = '_nolegend_'
        if scatter:
            # print('label', label)
            ax.scatter(X, Y, alpha=alpha, color=color, linewidth=lw, facecolors=facecolors, marker=marker, label=label)
        else:
            # print('label', label)
            if label is 'SAEM':
                linestyle = '--'
            ax.plot(X, Y, alpha=alpha, color=color, linewidth=lw, label=label, linestyle=linestyle)


    if plot_baseline:
        ignore = model(t0_m, tf_m, ID=0, ax=ax, baseline=True, plot=True, treatment=treatment)

    if return_ax:
        return df_grouped, ax
    plt.show()
    return df_grouped


# def group_and_plot(df, by='ID', x_label='Time', y_label='MTD', title='MTD vs Time'):
#     """ Group and plot
#     """
#     df_grouped = df.groupby(by)
#
#     for i in np.arange(np.max(df.loc[:, by])) + 1:
#         try:
#             Time = df_grouped.get_group(i).loc[:, x_label]
#             MTD = df_grouped.get_group(i).loc[:, y_label]
#             plt.plot(Time, MTD)
#         except KeyError:
#             pass
#
#     plt.xlabel(x_label)
#     plt.ylabel(y_label)
#     plt.title(title)
#
#     plt.show()
#
#     return df_grouped
