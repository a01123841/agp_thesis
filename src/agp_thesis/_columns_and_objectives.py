from .utilities import powerset


def columns_and_objectives(model_name, power, inverse_dt=False):

    if model_name == 'Ribbas':
        objectives = None
        if power is True:
            # train_columns = ['t', 'C', 'delta_t', '1_dt', '1_dt_2', '1_dt_3']
            train_columns = ['t', 'C', 'delta_t']
            list_powerset = list(powerset(train_columns))
            list_powerset = [list(set) for set in list_powerset]
            [element.append('MTD') for element in list_powerset]

            if inverse_dt:
                [element.append('1_dt') for element in list_powerset]

            powerset_names = ['-'.join(element) for element in list_powerset]
            col_options = dict(zip(powerset_names, list_powerset))
        else:
            train_columns = ['t', 'C', 'MTD']
            col_options = dict(zip(['t-C-MTD'], [train_columns]))

    elif model_name == 'Samads':
        train_columns = ['U', 't']
        if power is True:
            list_powerset = list(powerset(train_columns))
            list_powerset = [list(set) for set in list_powerset]
            [element.append('Y') for element in list_powerset]
            # [element.append('1_dt') for element in list_powerset]
            powerset_names = ['-'.join(element) for element in list_powerset]
            col_options = dict(zip(powerset_names, list_powerset))
            objectives = ['theta']
        else:
            train_columns = ['Y', 't']
            col_options = dict(zip(['Y-t'], [train_columns]))



    return train_columns, col_options, objectives
