from tensorflow import Session, reshape
from sklearn.model_selection import train_test_split
from keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import MinMaxScaler
from .constants import PARAMETERS_PCV_M
from .fit_plot_lognormal import fit_plot_lognormal
from .minmax_scaler_3d import minmax_scaler_3d
import numpy as np
import pandas as pd


def unshape_3d(array_3d, columns, IDs, drop_pad_values=True, pad_value=-100):
    pan = pd.Panel(array_3d)
    df = pan.swapaxes(0, 2).swapaxes(1,2).to_frame()
    df.columns = columns
    df.index.set_levels(IDs.values, level=0, inplace=True)
    df.index = df.index.droplevel('minor')

    df.reset_index(inplace=True)
    df.rename(columns={'major':'ID'}, inplace=True)
    if drop_pad_values:
        df = df.loc[df.MTD != pad_value]
    return df


def pop_all_first(samples, use_constraint=False):
    all_first = samples.groupby('ID').first().reset_index()
    if use_constraint:
        print('Constrain equation will be used for P0, predicting without the first  point in series.')
        duplicates = pd.concat([samples, all_first])
        samples = duplicates.drop_duplicates(keep=False)
    return samples, all_first


def scale_of_targets(y_train, y_test, y_columns, scale_type_targets, plot_confirmation):
    y_test_original = y_test
    # Transform
    if scale_type_targets == 'mean':
        y_train = np.asarray([y_train[:, i] / PARAMETERS_PCV_M[i] for i in np.arange(y_train.shape[1])]).transpose()
        y_test = np.asarray([y_test[:, i] / PARAMETERS_PCV_M[i] for i in np.arange(y_test.shape[1])]).transpose()
        # Transform back
        y_test_reversed = pd.DataFrame(
            data=np.asarray([y_test[:, i] * PARAMETERS_PCV_M[i] for i in np.arange(y_test.shape[1])]).transpose(),
            columns=y_columns)
        #             y_train = pd.DataFrame(data = y_train, columns = y_columns)
        #             y_test = pd.DataFrame(data = y_test, columns = y_columns)

        # plot confirmation of tranform
        n_bins = int(np.sqrt(len(y_train)))
        negative = False
        alpha = .3
        color = ['red', 'blue']
        #             color = ['red']
        if plot_confirmation:
            for i in np.arange(8):
                column = i
                data = [y_test_reversed.iloc[:, column], y_test_original.iloc[:, column]]
                title = 'Scaled target ' + y_columns[i]
                [x_fit_t_0, pdf_t_0, mu_t_0, sigma_t_0, counts_t_0, bin_edges_t_0] = fit_plot_lognormal(data,
                                                                                                        n_bins,
                                                                                                        title,
                                                                                                        negative=negative,
                                                                                                        alpha=alpha,
                                                                                                        color=color)
    if scale_type_targets == 'minmax':
        scaler = MinMaxScaler(copy=True, feature_range=(0, 1))
        scaler.fit(y_train)
        y_train = scaler.transform(y_train)

        scaler.fit(y_test)
        y_test = scaler.transform(y_test)

    return y_train, y_test


def scale_of_data(X_train, X_test):
    X_train = minmax_scaler_3d(X_train)
    X_test = minmax_scaler_3d(X_test)
    return X_train, X_test


def reshape_constant_length(samples, train_columns, N, n_p):
    samples = samples.loc[:, train_columns]
    samples3d_tf = reshape(samples, [N, n_p, len(train_columns)])
    sess = Session()
    with sess.as_default():
        samples3d = samples3d_tf.eval()
    samples3d.swapaxes(1, 2)
    return samples3d


def reshape_variable_length(samples, train_columns, max_length, pad_value=-100):
    train_columns.append('ID')
    samples = samples.loc[:, train_columns]
    matrix3d = np.array(list(samples.groupby('ID').apply(pd.DataFrame.as_matrix)))
    matrix3d_wo_ID = [x.swapaxes(0, 1)[:-1, :] for x in matrix3d]  # remove ID
    padded_list3d = np.array(
        [pad_sequences(patient, maxlen=max_length, value=pad_value, dtype='float64') for patient in matrix3d_wo_ID])
    samples3d = padded_list3d.swapaxes(1, 2)
    return samples3d


def reshape_3d(samples, var_length, train_columns, N, n_p, max_length, pad_value=-100):
    samples3d = []
    print('Shape of data to reshape:', samples.shape)
    if var_length == 0:
        samples3d = reshape_constant_length(samples, train_columns, N, n_p)
    elif var_length == 2:
        if max_length is None:
            max_length = max(samples.groupby('ID').count().MTD)
        print('Max length:', max_length)
        samples3d = reshape_variable_length(samples, train_columns, max_length, pad_value)
    return samples3d


def reshape_scale_split_data(samples, parameters, n_p, scale_targets=False, plot_confirmation=False,
                             train_columns=['MTD', 't', 'C'], objectives=None,
                             test_size=.3, random_state=0,
                             var_length=0,
                             pad_value=-100,
                             scale_type_targets='mean',
                             scale_data=False,
                             max_length=None,
                             use_constraint=False, shuffle=True):


    print('Reshaping...  w. number of points ', n_p)

    if any([col == 'delta_t' for col in train_columns]) or any([col == '1_dt' for col in train_columns]):
        samples = samples.dropna()
        samples['delta_t'] = samples.groupby('ID')['t'].transform(lambda x: x.diff())
        samples = samples.fillna(0)
        samples['1_dt'] = samples.groupby('ID')['delta_t'].transform(lambda x: 1/x)
        samples = samples.replace(np.inf, 0)

    ## 2.1 Reshape
    print('Preprocessing samples...')
    # Select, MTD, t and C or a combination
    parameters = parameters.loc[parameters.ID != 0]
    samples_wo_mean = samples.loc[samples.ID != 0]
    N = len(parameters)

    if objectives is not None:
        objectives.append('ID')
        parameters = parameters.loc[:, objectives]

    samples_wo_mean, all_first = pop_all_first(samples_wo_mean, use_constraint)

    if use_constraint and var_length == 0:
        n_p = n_p-1
    samples3d = reshape_3d(samples_wo_mean, var_length, train_columns, N, n_p, max_length, pad_value)

    ## 2.2 Targets and split:
    y_columns = parameters.drop('ID', axis=1).columns
    print('Samples 3D and Targets shapes:', samples3d.shape, parameters.shape)

    X_train, X_test, y_train, y_test = train_test_split(samples3d, parameters, test_size=test_size,
                                                        random_state=random_state, shuffle=shuffle)

    print('X/y train and X/y test shapes:', X_train.shape, y_train.shape, X_test.shape, y_test.shape)

    train_IDs = y_train.pop('ID')
    test_IDs = y_test.pop('ID')

    print('Shapes of train sets; X and y:', X_train.shape, y_train.shape)
    print('Shapes of test sets; X and y:', X_test.shape, y_test.shape)
    print('Number of nans found:')
    print(np.count_nonzero(np.isnan(X_train)))

    y_train = np.array(y_train)
    y_test = np.array(y_test)

    if scale_targets:
        y_train, y_test = scale_of_targets(y_train, y_test, y_columns, scale_type_targets, plot_confirmation)

    if scale_data:
        X_train, X_test = scale_of_data(X_train, X_test)

    return X_train, X_test, y_train, y_test, y_columns, train_IDs, test_IDs, all_first
