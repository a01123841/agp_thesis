from .constants import *
from .assess_model import assess_model
from .reshape_scale_split_data import reshape_3d, pop_all_first
from .assess_model import constraint_for_P0, first_MTD_RMSE


def preprocess_real_data(train_columns, processed_real_data_path, samples=None):
    # Remove Nans
    save = False
    if samples is None:
        samples = PCV_df_shift
        samples = PCV_df_shift[np.isfinite(PCV_df_shift['MTD'])]
        # Change column names
        columns = samples.columns.values
        columns[1] = 't'
        samples.columns = columns
        # Remove all t=0 points, missing MTDs
        samples = samples.loc[samples.t != 0]
        save = True

    # Obtain  delta of MTD for imputing NaNs
    samples['delta_MTD'] = samples.groupby('ID')['MTD'].transform(lambda x: x.diff())
    samples['delta_MTD'][:-2] = samples['delta_MTD'][2:]
    if any([col == 'delta_t' for col in train_columns]) or any([col == '1_dt' for col in train_columns]):
        samples['delta_t'] = samples.groupby('ID')['t'].transform(lambda x: x.diff())
        samples = samples.fillna(0)
        samples['1_dt'] = samples.groupby('ID')['delta_t'].transform(lambda x: 1 / x)
        samples = samples.replace(np.inf, 0)
    print(samples)
    if save:
        samples.to_pickle(processed_real_data_path)
    return samples


def real_data_assess_and_predict(folder_path, data_path, processed_data_fn, weights_fn,
                                 train_columns, var_length,
                                 max_length, pad_value, drop_rate=0,
                                 loss='mape',
                                 layers=False,
                                 compress=False,
                                 hidden_units=256,
                                 activation='prelu',
                                 error='mape',
                                 y_test=None,
                                 n_p=17,
                                 treatment=True,
                                 scale_targets=False,
                                 use_constraint=False,
                                 samples = None,
                                 batch_norm=False):
    processed_real_data_path = os.path.join(data_path, processed_data_fn)
    if samples is None:
        if os.path.isfile(processed_real_data_path):
            print('Processed real data set already exists, loading...')
            samples = pd.read_pickle(os.path.join(data_path, processed_data_fn))
            if any(samples.columns == '1_dt'):
                samples = preprocess_real_data(train_columns, processed_real_data_path, samples)
        else:
            print('Processed real data set not found, processing...')
            samples = preprocess_real_data(train_columns, processed_real_data_path)

    N = max(samples.ID.values)

    if use_constraint:
        # Done here for real data only. Normally, the generator handles this
        samples, all_first = pop_all_first(samples)
    else:
        all_first = None

    # Real data
    X_test = reshape_3d(samples, var_length, train_columns, N, n_p, max_length, pad_value)
    # print(X_test)
    print('X_test shape:', X_test.shape)
    input_shape = (max_length, X_test.shape[2])

    # Since the model was trained with scaled targets, y_test is scaled
    best_model, y_prime, y_prime_unscaled, errors, rmse_first = assess_model(folder_path, weights_fn, X_test, y_test,
                                                                            input_shape, drop_rate, activation=activation,
                                                                            hidden_units=hidden_units, loss=loss,
                                                                            layers=layers, compress=compress,
                                                                            error=error,
                                                                            treatment=treatment,
                                                                            scale_targets=scale_targets,
                                                                            use_constraint=use_constraint,
                                                                            all_first=all_first,
                                                                            batch_norm=batch_norm,
                                                                            var_length=var_length,
                                                                            pad_value=pad_value)
    print('RMSE first', rmse_first)
    IDs = np.arange(y_prime.shape[0]) + 1

    if scale_targets:
        y_prime = y_prime_unscaled
    if not use_constraint and all_first is None:
        samples, all_first = pop_all_first(samples)
        rmse = first_MTD_RMSE(all_first, y_prime)
    y_prime['ID'] = IDs

    return samples, X_test, y_test, y_prime, best_model, errors
