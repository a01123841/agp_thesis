def create_id(N, n_p, dt = None, treatment=True, uniform_dt=True, noise=True,
              current_sigma=0, it=0, gpu=False, add_sigma=True,
              model_name='Ribbas', multi_treat=False, free_n_treat=False, **others):

    if n_p is False or n_p is None:
        sample_method = '_dt'
        sample_value = dt
    else:
        sample_method = '_np'
        sample_value = n_p

    if model_name == 'Samads':
        # identifier = 'SAM' + '_N' + str(N) + '_it' + str(it) + sample_method + str(sample_value) + \
        #              '_dtdif' + str(int(uniform_dt)) + \
        #              '_noise' + str(int(noise))
        identifier = 'SAM' + '_N' + str(N) + '_it' + str(it) + sample_method + str(sample_value) + \
                     '_dtdif' + str(int(uniform_dt))
    else:
        # identifier = '_N' + str(N) + '_it' + str(it) + sample_method + str(sample_value) + \
        #              '_dtdif' + str(int(uniform_dt)) + '_tr' + str(int(treatment)) + \
        #              '_noise' + str(int(noise))
        identifier = '_N' + str(N) + '_it' + str(it) + sample_method + str(sample_value) + \
                     '_dtdif' + str(int(uniform_dt)) + '_tr' + str(int(treatment))

    if add_sigma:
        identifier = identifier + '_sig' + str(current_sigma)

    if gpu:
        identifier = identifier + 'gpu'

    if multi_treat:
        if not free_n_treat:
            identifier = identifier + '_mt'
        if free_n_treat:
            identifier = identifier + '_freet'

    return identifier
