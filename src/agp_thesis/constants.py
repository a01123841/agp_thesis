## ###########    PCV PARAMETERS     #################################
import pandas as pd
import numpy as np
from .mspe_loss import mspe_loss
from .mape_constrained_loss import mape_constrained_loss, mape_constrained_loss_no_treat
from .thesis_errors import *
from .sample_rand_parameter_logn import underlying_normal
from keras.layers import ELU
from keras.layers.advanced_activations import LeakyReLU, PReLU, ThresholdedReLU
import os


"""
In here you can find hardcoded all necessary values, functions, datasets, etc. that will be ready when loading the
library.   
    

"""

cwd = os.getcwd()
parent_dir = os.path.abspath(os.path.join(cwd, os.pardir))
data_folder = os.path.join(parent_dir, 'Data')
monolix_data_path = '/home/galethog/LRZ_Sync+Share/Thesis/SAEM_andres/SAEM_andres/data/tumor'


pre_fn = 'PCV_validation_pretreatment'
pos_fn = 'PCV_validation_postreatment'
PCV_val_fn = 'PCV_validation_set'
PCV_pre1_fn = 'PCV_preprocessed_1'
PCV_pre2_fn = 'PCV_preprocessed_2'
PCV_pre3_fn = 'PCV_preprocessed_3'
PCV_pre4_fn = 'PCV_preprocessed_C_nans'

pre_treat_df = pd.read_pickle(os.path.join(data_folder, pre_fn))
pos_treat_df = pd.read_pickle(os.path.join(data_folder, pos_fn))
PCV_df_shift = pd.read_pickle(os.path.join(data_folder, PCV_val_fn))
PCV_df_pre_1 = pd.read_pickle(os.path.join(data_folder, PCV_pre1_fn))
PCV_df_pre_2 = pd.read_pickle(os.path.join(data_folder, PCV_pre2_fn))
PCV_df_pre_3 = pd.read_pickle(os.path.join(data_folder, PCV_pre3_fn))
PCV_df_pre_4 = pd.read_pickle(os.path.join(data_folder, PCV_pre4_fn))



none_options = {'' : None}

error_options = {"mse": mean_squared_error,
                 "mape": mean_absolute_percentage_error,
                 "rmse": root_mean_squared_error
                 # "chi_square": pearson_chi_square,
                 }

loss_options = {"mse_loss" : 'mse',
                "mape_loss" : 'mape',
                "mspe_loss" : mspe_loss,
                "mape_loss_c" : mape_constrained_loss,
                "mape_loss_c_no_treat": mape_constrained_loss_no_treat
                }

activation_options = {"linear" : "linear",
                    "relu" : "relu",
                    "elu" : ELU(),
                    "prelu" : PReLU(alpha_initializer='zeros', alpha_regularizer=None, alpha_constraint=None, shared_axes=None),
                    "leaky_relu" : LeakyReLU(alpha=.2),
                    "trelu" : ThresholdedReLU()
                    }

N_options = {'10' : 10,
             '100' : 100,
             '1000' : 1000,
             '10000' : 10000,
             '100000' : 100000
             }

n_p_options = {'6': 6,
              '12': 12,
              '18': 18,
              '24': 24,
              '30': 30
              }

batch_size = [8, 16, 32, 64, 128, 256, 512] # [1024, 2048]
batch_keys = [str(x) for x in batch_size]

batch_options = dict(zip(batch_keys, batch_size))

hidden_units = [16, 32, 64, 128, 256, 512] # [1024, 2048]
units_keys = [str(x) for x in hidden_units]

unit_options = dict(zip(units_keys,hidden_units))

# MEAN VALUES (parameter estimates):
P0_pcv_m =       7.13  # [mm]
Q0_pcv_m =       41.2  # [mm]
lambda_p_pcv_m = 0.121 # [mo^-1]
k_PQ_pcv_m =     0.0295 # [mo^-1]
k_Qp_P_pcv_m =   0.0031 # [mo^-1]
delta_Qp_pcv_m = 0.00867 # [mo^-1]
gamma_pcv_m =    0.729  #[]
KDE_pcv_m =      0.24 # [mo^-1]


PARAMETERS_PCV_M = [P0_pcv_m, Q0_pcv_m, lambda_p_pcv_m, k_PQ_pcv_m, k_Qp_P_pcv_m, delta_Qp_pcv_m, gamma_pcv_m, KDE_pcv_m]

# ERROR VALUES of mean distributions (parenthersis in paper):
P0_pcv_me =          25  # [mm]
Q0_pcv_me =          7  # [mm]
lambda_p_pcv_me =    16 # [mo^-1]
k_PQ_pcv_me =        21 # [mo^-1]
k_Qp_P_pcv_me =      35 # [mo^-1]
delta_Qp_pcv_me =    21 # [mo^-1]
gamma_pcv_me =       37  #[]
KDE_pcv_me =         33 # [mo^-1]

PARAMETERS_PCV_ME = [P0_pcv_me, Q0_pcv_me, lambda_p_pcv_me, k_PQ_pcv_me, k_Qp_P_pcv_me, delta_Qp_pcv_me, gamma_pcv_me, KDE_pcv_me]

# INTERINDIVIDUAL VARIABILITY (CV = ration of sigma/mu:
P0_pcv_cv =          94  # [mm]
Q0_pcv_cv =          54  # [mm]
lambda_p_pcv_cv =    72 # [mo^-1]
k_PQ_pcv_cv =        76 # [mo^-1]
k_Qp_P_pcv_cv =      97 # [mo^-1]
delta_Qp_pcv_cv =    75 # [mo^-1]
gamma_pcv_cv =       115  #[]
KDE_pcv_cv =         70 # [mo^-1]

PARAMETERS_PCV_CV = [P0_pcv_cv, Q0_pcv_cv, lambda_p_pcv_cv, k_PQ_pcv_cv, k_Qp_P_pcv_cv, delta_Qp_pcv_cv, gamma_pcv_cv, KDE_pcv_cv]

# INTERINDIVIDUAL VARIABILITY of mean distributions (parenthersis in paper):
P0_pcv_cve =          23  # [mm]
Q0_pcv_cve =          10  # [mm]
lambda_p_pcv_cve =    9 # [mo^-1]
k_PQ_pcv_cve =        12 # [mo^-1]
k_Qp_P_pcv_cve =      91 # [mo^-1]
delta_Qp_pcv_cve =    12 # [mo^-1]
gamma_pcv_cve =       9  #[]
#     KDE_pcv_cve =          # [mo^-1]

PARAMETERS_PCV_CVE = [P0_pcv_cve, Q0_pcv_cve, lambda_p_pcv_cve, k_PQ_pcv_cve, k_Qp_P_pcv_cve, delta_Qp_pcv_cve, gamma_pcv_cve]

#
# # ERROR VALUES (parenthersis in paper):
#     t0_e =      40.175  # [mo]
#     tf_e =      36.136  # [mo]
#     dt_e =      7.923 # [mo]
#     n_p_e =     3.8902

# MU VALUES (LOGNORMAL PARAMETERS):
t0_mu =      13.39503200422206  # [mo]
tf_mu =      51.307819208756968 # [mo]
dt_mu =      3.2061616747872002 # [mo]
n_p_mu =     17.761 # Gaussiangr

# MEAN VALUES:
t0_m =      -37.509 # [mo]
tf_m =      60.121  # [mo]
dt_m =      5.82    # [mo]
n_p_m =     17.761  # Gaussian

# ERROR VALUES (parenthersis in paper):
t0_sigma =      2.0046540511302346  # [mo]
tf_sigma =      0.56801575369914814  # [mo]
dt_sigma =      1.1812172985514979 # [mo]
n_p_sigma =     3.8902

# Underlying Gaussian values of Lognormal distributions:

# t0_under_m, t0_under_s = underlying_normal(t0_mu, t0_sigma)
t0_under_m, t0_under_s = underlying_normal(np.abs(t0_m), t0_sigma)

# tf_under_m, tf_under_s = underlying_normal(tf_mu, tf_sigma)
tf_under_m, tf_under_s = underlying_normal(tf_m, tf_sigma)

# dt_under_m, dt_under_s = underlying_normal(dt_mu, dt_sigma)
dt_under_m, dt_under_s = underlying_normal(dt_m, dt_sigma)

# n_p_under_m, n_p_under_s = underlying_normal(n_p_mu, n_p_sigma)
n_p_under_m, n_p_under_s = underlying_normal(n_p_m, n_p_sigma)