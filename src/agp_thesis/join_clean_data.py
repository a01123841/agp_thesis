import os
import pandas as pd
import numpy as np


def _clean_dif_length(samples, parameters, n_p, samples_noised=None):
    # Removing n != n_p
    IDs_remove = samples.groupby('ID').count().MTD.loc[
        samples.groupby('ID').count().MTD != n_p].index.values
    IDs_mantain = samples.groupby('ID').count().MTD.loc[
        samples.groupby('ID').count().MTD == n_p].index.values

    print('Removing ', len(IDs_remove), ' IDs')

    clean_samples = samples[samples['ID'].isin(IDs_mantain)]
    clean_parameters = parameters[parameters['ID'].isin(IDs_mantain)]

    if samples_noised is not None:
        clean_samples_noised = samples_noised[samples_noised['ID'].isin(IDs_mantain)]
    else:
        clean_samples_noised = None

    return clean_samples, clean_parameters, clean_samples_noised


def join_clean_data(folder='Pool_big', identifiers=['_N100','_N200'], noise_identifiers=['_N100','_N200'], save_identifier='_N300',
                    noise_save_identifier = '_N300', n_p=17,
                    clean_length=True, save=True, noise=False):

    samples_final = pd.DataFrame()
    parameters_final = pd.DataFrame()
    samples_noised_final = None
    fname_samples_noised = None
    temp_max_ID = 0

    fname_parameters = os.path.join(folder, 'art_parameters' + save_identifier)
    fname_samples = os.path.join(folder,  'art_samples' + save_identifier)

    if noise:
        fname_samples_noised = os.path.join(folder,  'art_samples_noised' + noise_save_identifier)

    if not os.path.isfile(fname_samples):
        print('Generating samples and paramaters with identifier:', save_identifier)
        for i, identifier in enumerate(identifiers):
            # Load
            print('Loading files with identifier ', identifier)
            temp_samples = pd.read_pickle(os.path.join(folder,  'art_samples' + identifier))
            temp_parameters = pd.read_pickle(os.path.join(folder, 'art_parameters' + identifier))

            # remove ID==0
            temp_samples = temp_samples.loc[temp_samples.ID != 0]
            temp_parameters = temp_parameters.loc[temp_parameters.ID != 0]

            if noise:
                temp_samples_noised = pd.read_pickle(os.path.join(folder, 'art_samples_noised' + noise_identifiers[i]))
                temp_samples_noised = temp_samples.loc[temp_samples_noised.ID != 0]

            if clean_length:
                print('Cleaning...')
                temp_samples, temp_parameters, temp_samples_noised = _clean_dif_length(temp_samples, temp_parameters,
                                                                                           n_p, temp_samples_noised)

            print('Samples:', temp_samples.shape)
            print('Parameters:', temp_parameters.shape)
            # replace index
            temp_samples['ID'] = temp_samples['ID'] + temp_max_ID
            temp_parameters['ID'] = temp_parameters['ID'] + temp_max_ID
            # append
            samples_final = samples_final.append(temp_samples)
            parameters_final = parameters_final.append(temp_parameters)

            if noise:
                print('Samples noised:', temp_samples_noised.shape)
                # replace index
                temp_samples_noised['ID'] = temp_samples_noised['ID'] + temp_max_ID
                # append
                samples_noised_final = samples_noised_final.append(temp_samples_noised)

            temp_max_ID = np.int(max(parameters_final.ID))
            print(temp_max_ID)

    else:
        print('File already exists, loading files for identifier', save_identifier)
        print('Filename of samples:', fname_samples)
        samples_final = pd.read_pickle(fname_samples)
        parameters_final = pd.read_pickle(fname_parameters)
        if noise:
            print('Filename of samples noised:', fname_samples)
            samples_noised_final = pd.read_pickle(fname_samples_noised)

        if clean_length:
            print('Cleaning...')
            samples_final, parameters_final, samples_noised_final = _clean_dif_length(samples_final, parameters_final,
                                                                                      n_p, samples_noised_final)
    if save:
        pd.to_pickle(samples_final, os.path.join(folder, fname_samples))
        pd.to_pickle(parameters_final, os.path.join(folder, fname_parameters))
        if noise:
            pd.to_pickle(samples_noised_final, os.path.join(folder, fname_samples_noised))

    print('Final shapes samples and parameters ', samples_final.shape, parameters_final.shape)
    if noise:
        print('Final shape samples noised', samples_noised_final.shape)

    return samples_final, parameters_final, samples_noised_final
