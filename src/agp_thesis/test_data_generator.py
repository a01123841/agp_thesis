from ._columns_and_objectives import columns_and_objectives
from .label_load_save_artificial_data import label_load_save_artificial_data
from .reshape_scale_split_data import reshape_scale_split_data
from .join_clean_data import join_clean_data
import os
import pandas as pd


def test_data_generator(generation_kw, generation_opts, plot_opts,
                        N_per_file, noise_sigma,
                        gpu,
                        pool_path, folder_path,
                        model_name, power,
                        scale_targets,
                        test_size,
                        save_df,
                        inverse_dt,
                        activation='prelu',
                        cv_options={'' : None},
                        clean=True,
                        random_state=0,
                        d_rows_sigma=None,
                        max_length=30,
                        var_length=0,
                        pad_value=-100,
                        scale_type_targets='mean',
                        scale_data=False,
                        use_constraint=False, shuffle=True,
                        multi_treat=False,
                        return_X_df=False,
                        col_options = None,
                        free_n_treat = False,
                        del_t=.05):
    """
        Generation function that wraps the first part of script where data is created, loaded, saved and
        preprocessed. Every time a new data set is returned according to the next iteration of the cv_options,
        col_options and noise_sigma, in that order.


    Args:
        generation_kw:
        generation_opts:
        plot_opts:
        N_per_file:
        noise_sigma:
        gpu:
        pool_path:
        folder_path:
        model_name:
        power:
        scale_targets:
        test_size:
        save_df:
        inverse_dt:
        activation:
        cv_options:
        clean:
        random_state:
        d_rows_sigma:
        max_length:
        var_length:
        pad_value:
        scale_type_targets:
        scale_data:
        use_constraint:

    Returns:
        (X_train, X_test, y_train, y_test, y_columns, train_IDs, test_IDs, identifier, all_first[0])
    """
    if col_options is None:
        train_columns, col_options, objectives = columns_and_objectives(model_name, power, inverse_dt=inverse_dt)
        if not power and inverse_dt:
            col_options = {'t-delta_t-MTD-1_dt': ['t', 'delta_t', 'MTD', '1_dt']}
    else:
        train_columns, _, objectives = columns_and_objectives(model_name, power, inverse_dt=inverse_dt)

    samples_original = []
    n_p = generation_kw['n_p']
    noise = generation_kw['noise']
    for current_sigma in noise_sigma:
        if not noise:
            current_sigma = 0
        parameters, samples, samples_noised, curves, identifier, noise_identifier =\
            label_load_save_artificial_data(generation_kw, generation_opts, plot_opts,
                                            N_per_file = N_per_file, current_sigma=current_sigma,
                                            gpu=gpu, folder=pool_path, N_plot_preview=15,
                                            model_name=model_name, unify=True,
                                            d_rows_sigma=d_rows_sigma,
                                            max_length=max_length,
                                            multi_treat=multi_treat,
                                            free_n_treat=free_n_treat,
                                            del_t=del_t)
        if clean:
            samples, parameters, samples_noised = join_clean_data(folder=pool_path, identifiers=[identifier], noise_identifiers=[noise_identifier],
                                                                  save_identifier=identifier, noise_save_identifier=noise_identifier, n_p=n_p,
                                                                  clean_length=True, save=True, noise=noise)
        if noise:
            print('Noise ', noise, ' with a sigma of ', current_sigma, '; using noised samples.')
            # samples_original = samples
            samples_original = samples
            samples = samples_noised

        for _, train_columns in  col_options.items():
            X_train, X_test, y_train, y_test, y_columns, train_IDs, test_IDs, all_first = \
                reshape_scale_split_data(samples, parameters, n_p=n_p,
                                         scale_targets=scale_targets, plot_confirmation=False,
                                         train_columns=train_columns, objectives=objectives,
                                         test_size=test_size, random_state=random_state,
                                         var_length=var_length,
                                         pad_value=pad_value,
                                         scale_type_targets=scale_type_targets,
                                         scale_data=scale_data, max_length=max_length,
                                         use_constraint=use_constraint, shuffle=shuffle)

            for cv_key, cv_none in cv_options.items():
                if save_df:
                    test_targets = pd.DataFrame(data = y_test, columns = y_columns)
                    if cv_key is not None and cv_key is not '':
                        identifier = '_' + cv_key +  identifier
                        noise_identifier = '_' + cv_key + noise_identifier
                    train_IDs.to_pickle(os.path.join(folder_path, 'train_IDs_assess' + '_' + identifier))
                    test_IDs.to_pickle(os.path.join(folder_path, 'test_IDs_assess' + '_'  + identifier))
                    test_targets.to_pickle(os.path.join(folder_path, 'y_true_assess' + '_' + activation + identifier))

            if return_X_df and not noise:
                yield (X_train, X_test, y_train, y_test, y_columns, train_IDs, test_IDs, identifier, all_first, samples)
            if noise and return_X_df:
                yield (X_train, X_test, y_train, y_test, y_columns, train_IDs, test_IDs, identifier, noise_identifier, all_first,
                       samples_original, samples_noised)
            else:
                yield (X_train, X_test, y_train, y_test, y_columns, train_IDs, test_IDs, identifier, all_first)
