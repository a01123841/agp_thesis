from .constants import *
from ._dynamical_model_parameters import model_parameters
from .plot_utils import group_and_scatter
from .gen_artificial_data import add_noise
import matplotlib.pyplot as plt
import pandas as pd
from tqdm import tqdm

from scipy.stats import truncnorm

#plt.style.use('ggplot')

# ERROR VALUES of mean distributions (parenthersis in paper):
# P0_pcv_me =          25  # [mm]
# Q0_pcv_me =          7  # [mm]
# lambda_p_pcv_me =    16 # [mo^-1]
# k_PQ_pcv_me =        21 # [mo^-1]
# k_Qp_P_pcv_me =      35 # [mo^-1]
# delta_Qp_pcv_me =    21 # [mo^-1]
# gamma_pcv_me =       37  #[]
# KDE_pcv_me =         33 # [mo^-1]
#
# PARAMETERS_PCV_ME = [P0_pcv_me, Q0_pcv_me, lambda_p_pcv_me, k_PQ_pcv_me, k_Qp_P_pcv_me, delta_Qp_pcv_me, gamma_pcv_me, KDE_pcv_me]


MTD_pcv_me = P0_pcv_me + Q0_pcv_me

def add_noise_samples(samples, noise_sigma=5, noise_type='Normal', plot_samples=True, N_plot=15,
                        plot_title="Noisy Curves with different parameters", model_name='Ribbas',
                        **others):
    """

    :param samples:
    :param noise_sigma:
    :param noise_type:
    :param plot_samples:
    :param N_plot:
    :param plot_title:
    :param model_name:
    :param others:
    :return:
    """

    samples = samples.loc[samples.ID != 0]
    IDs = samples.ID.unique()
    grouped_samples = samples.groupby("ID")
    samples_noised = pd.DataFrame()
    
    t0, tf, x_label, y_label, _, xmin, xmax, ymin, ymax, title = model_parameters(model_name)

    for ID in tqdm(IDs):
        SAMPLE = grouped_samples.get_group(ID)
        SAMPLE_NOISED = SAMPLE.copy()
        noised = add_noise(sigma=noise_sigma, type=noise_type, length=len(SAMPLE))
        SAMPLE_NOISED.loc[:, y_label] = SAMPLE_NOISED.loc[:, y_label] + noised

        samples_noised = pd.concat([samples_noised, SAMPLE_NOISED])

    # samples_noised.dropna(inplace=True)  # dropna or coerce

    if plot_samples:
        ignore = group_and_scatter(samples_noised, N=N_plot, x_label=x_label, y_label=y_label, title=plot_title,
                                   plot_baseline=False, scatter=True, ax=None, model_name=model_name, xmin=xmin,
                                   xmax=xmax, ymin=ymin, ymax=ymax)
    return samples_noised

