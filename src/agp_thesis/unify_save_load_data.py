from .create_id import create_id
from .plot_utils import group_and_scatter
from ._dynamical_model_parameters import model_parameters
import numpy as np
import pandas as pd
import os


def unify_save_load_data(generation_kw, generation_opts, plot_opts, N_per_file=40000,
                         current_sigma=0, gpu=False,
                         folder='./Data/Pool', N_plot_preview=15,
                         model_name='Ribbas',
                         save=True, multi_treat=False,
                         free_n_treat=False,
                         **others):
    N = generation_kw['N']
    noise = generation_kw['noise']
    return_curves = generation_opts['return_curves']

    # print(noise, current_sigma)

    samples_noised = []
    curves = []
    t0, tf, x_label, y_label, del_t, xmin, xmax, ymin, ymax, title = model_parameters(model_name)

    times = int(np.ceil(N / N_per_file))
    base_identifier = create_id(**generation_kw, current_sigma=current_sigma, it=0, gpu=gpu, add_sigma=False,
                                model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)
    noise_base_identifier = create_id(**generation_kw, current_sigma=current_sigma, it=0, gpu=gpu, add_sigma=True,
                                model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)

    print(base_identifier)
    fname_samples = os.path.join(folder, 'art_samples' + base_identifier)

    if os.path.isfile(fname_samples):
        print('A unified file seems to exist for identifier ', base_identifier)
        times = 0
        save = False

        fname_parameters = os.path.join(folder, 'art_parameters' + base_identifier)
        fname_samples_noised = os.path.join(folder, 'art_samples_noised' + noise_base_identifier)
        # fname_curves = os.path.join(folder, 'art_curves' + base_identifier)

        if noise:
            samples_noised = pd.read_pickle(fname_samples_noised)
        samples = pd.read_pickle(fname_samples)
        parameters = pd.read_pickle(fname_parameters)
        samples.head()
        parameters.head()
        print('Samples and Parameters shapes:', samples.shape, parameters.shape)
    else:
        print('Appending and unifying files for identifier ', base_identifier)

        # load first
        identifier = create_id(**generation_kw, current_sigma=current_sigma, it=1, gpu=gpu, add_sigma=False,
                               model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)
        noise_identifier = create_id(**generation_kw, current_sigma=current_sigma, it=1, gpu=gpu, add_sigma=True,
                               model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)
        print(identifier)
        fname_parameters = os.path.join(folder, 'art_parameters' + identifier)
        fname_samples = os.path.join(folder, 'art_samples' + identifier)
        fname_samples_noised = os.path.join(folder, 'art_samples_noised' + noise_identifier)
        # fname_curves = os.path.join(folder, 'art_curves' + identifier)

        if noise:
            samples_noised = pd.read_pickle(fname_samples_noised)
            print('HERE',len(samples_noised))
        samples = pd.read_pickle(fname_samples)
        parameters = pd.read_pickle(fname_parameters)

        for it in np.arange(times)[1:] + 1:
            identifier = create_id(**generation_kw, current_sigma=current_sigma, it=int(it), gpu=gpu, add_sigma=False,
                                   model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)
            noise_identifier = create_id(**generation_kw, current_sigma=current_sigma, it=int(it), gpu=gpu, add_sigma=True,
                                   model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)
            print(identifier)
            fname_parameters = os.path.join(folder, 'art_parameters' + identifier)
            fname_samples = os.path.join(folder, 'art_samples' + identifier)
            fname_samples_noised = os.path.join(folder, 'art_samples_noised' + noise_identifier)
            # fname_curves = os.path.join(folder, 'art_curves' + identifier)

            # load and append:
            if noise:
                samples_noised = samples_noised.append(pd.read_pickle(fname_samples_noised))
            samples = samples.append(pd.read_pickle(fname_samples))
            parameters = parameters.append(pd.read_pickle(fname_parameters))
            print(parameters.shape)

    # Plot first N series from the sampled data
    if noise:
        ignore = group_and_scatter(samples_noised, x_label=x_label, y_label=y_label, title=title, scatter=False,
                                   ax=None, N=N_plot_preview, plot_baseline=False,
                                   model_name=model_name, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    ignore = group_and_scatter(samples, x_label=x_label, y_label=y_label, title=title, scatter=True, ax=None,
                               N=N_plot_preview, plot_baseline=False,
                               model_name=model_name, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)

    if save:
        fname_samples = os.path.join(folder, 'art_samples' + base_identifier)
        fname_parameters = os.path.join(folder, 'art_parameters' + base_identifier)
        fname_samples_noised = os.path.join(folder, 'art_samples_noised' + noise_base_identifier )
        fname_curves = os.path.join(folder, 'art_curves' + base_identifier)
        print('Saving dataframes with identifier:', base_identifier)
        if noise or current_sigma != 0:
            samples_noised.to_pickle(fname_samples_noised)
        if return_curves:
            curves.to_pickle(fname_curves)
        samples.to_pickle(fname_samples)
        parameters.to_pickle(fname_parameters)

    parameters = parameters.reset_index(drop=True)
    samples = samples.reset_index(drop=True)
    if noise or current_sigma != 0:
        samples_noised.reset_index(drop=True, inplace=True)
    print('Samples and Parameters shapes:', samples.shape, parameters.shape)
    # print( len(samples_noised))
    return parameters, samples, samples_noised, base_identifier
