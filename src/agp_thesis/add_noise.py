import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from .constants import *

# ERROR VALUES of mean distributions (parenthersis in paper):
# P0_pcv_me =          25  # [mm]
# Q0_pcv_me =          7  # [mm]
# lambda_p_pcv_me =    16 # [mo^-1]
# k_PQ_pcv_me =        21 # [mo^-1]
# k_Qp_P_pcv_me =      35 # [mo^-1]
# delta_Qp_pcv_me =    21 # [mo^-1]
# gamma_pcv_me =       37  #[]
# KDE_pcv_me =         33 # [mo^-1]
#
# PARAMETERS_PCV_ME = [P0_pcv_me, Q0_pcv_me, lambda_p_pcv_me, k_PQ_pcv_me, k_Qp_P_pcv_me, delta_Qp_pcv_me, gamma_pcv_me, KDE_pcv_me]

MTD_pcv_me = P0_pcv_me + Q0_pcv_me

def add_noise(mean=0, sigma = MTD_pcv_me, length = 17, type='Normal'):
    # Adding noise to curves MTD, P, and Q. Since
    noise_vector = []
    if type is 'Normal':
        noise_vector = np.random.normal(mean, sigma, length)

    if type is 'Lognormal':
        noise_vector = np.random.normal(mean, sigma, length)

    return noise_vector


# def add_noise(mean=0, sigma= [P0_pcv_me, Q0_pcv_me], type='Normal'):
#     # Adding noise to curves MTD, P, and Q. Since
#     if mean == 0:
#         mean = np.zeros(len(sigma))
#     if type is 'Normal':
#         noise_vector = np.random.normal(mean, sigma)
#         noise_vector = np.append(sum(noise_vector), noise_vector)
# 
#     if type is 'Lognormal':
#         noise_vector = np.random.normal(mean, sigma)
#         noise_vector = np.append(sum(noise_vector), noise_vector)
# 
#     noise_vector = np.append(np.append(np.zeros(1), noise_vector), np.zeros(4))
# 
#     return noise_vector