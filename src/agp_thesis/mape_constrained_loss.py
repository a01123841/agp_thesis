# option 3B
import tensorflow as tf
from keras import backend as K
import numpy as np


# Note: apparently this works for Training, but doesnt work for Testing. This is because MTD0 is
# different for training than for testing

def mape_constrained_loss(y_true, y_pred, MTD0=None):

    MTD = tf.gather(y_true, [8], axis=1)  # option 3: gather
    print('Shape of MTD', MTD.shape)  # (128, 1)

    ones_matrix = K.ones([128, K.int_shape(y_pred)[1] - 1], dtype=tf.float32)  # check
    zeros_column = K.zeros([128, 1], dtype=tf.float32)  # check
    mask_ones = tf.concat([zeros_column, ones_matrix], 1)  # check
    print('Shape mask ones', K.int_shape(mask_ones))  # (128, 8)

    zeroes_matrix = K.zeros([128, K.int_shape(y_pred)[1] - 1], dtype=tf.float32)
    print('Type of slice of y_pred', type(y_pred[:, 1]))

    Q0 = tf.gather(y_pred, [1], axis=1)  # option 3: gather
    P0 = tf.gather(y_pred, [0], axis=1)  # option 3B

    #     print('Shape of P0 reshaped', P0.shape) # (128, 1)
    #     print('y_true sliced shape', K.int_shape(y_true[:,-1]))
    #     print('Shape mask P0', K.int_shape(mask_P0))     # (128, 8)


    # #     print('Type y_true:', type(y_true))
    # #     print('Type y_pred:', type(y_pred))
    y_true_sliced = tf.gather(y_true, [0, 1, 2, 3, 4, 5, 6, 7], axis=1)
    #     print('Shape y sliced', K.int_shape(y_true_sliced)) # (None, None) # corrected: (None, 8)
    diff = y_pred - y_true_sliced
    #     print('Shape diff', K.int_shape(diff)) # (None, 8)
    #     print('Shape loss', K.int_shape(loss)) # Shape loss (128, 8)

    # return K.mean(tf.concat([tf.divide(K.abs(diff), y_true_sliced), tf.divide(P0 + Q0, MTD)], 1))  # option 2
    # return K.sum(K.mean(tf.divide(K.abs(diff), y_true_sliced)), tf.divide(P0 + Q0, MTD))  # option plus
    return K.sum(K.mean(tf.divide(K.abs(diff), y_true_sliced)), tf.divide(K.abs(P0+Q0-MTD), MTD))  # option data reply


def mape_constrained_loss_no_treat(y_true, y_pred, MTD0=None):

    MTD = tf.gather(y_true, [4], axis=1)  # option 3: gather
    print('Shape of MTD', MTD.shape)  # (128, 1)

    ones_matrix = K.ones([128, K.int_shape(y_pred)[1] - 1], dtype=tf.float32)  # check
    zeros_column = K.zeros([128, 1], dtype=tf.float32)  # check
    mask_ones = tf.concat([zeros_column, ones_matrix], 1)  # check
    print('Shape mask ones', K.int_shape(mask_ones))  # (128, 8)

    zeroes_matrix = K.zeros([128, K.int_shape(y_pred)[1] - 1], dtype=tf.float32)
    print('Type of slice of y_pred', type(y_pred[:, 1]))

    Q0 = tf.gather(y_pred, [1], axis=1)  # option 3: gather
    P0 = tf.gather(y_pred, [0], axis=1)  # option 3B

    #     print('Shape of P0 reshaped', P0.shape) # (128, 1)
    #     print('y_true sliced shape', K.int_shape(y_true[:,-1]))
    #     print('Shape mask P0', K.int_shape(mask_P0))     # (128, 8)

    # #     print('Type y_true:', type(y_true))
    # #     print('Type y_pred:', type(y_pred))
    y_true_sliced = tf.gather(y_true, [0, 1, 2, 3], axis=1)
    #     print('Shape y sliced', K.int_shape(y_true_sliced)) # (None, None) # corrected: (None, 8)
    diff = y_pred - y_true_sliced
    #     print('Shape diff', K.int_shape(diff)) # (None, 8)
    #     print('Shape loss', K.int_shape(loss)) # Shape loss (128, 8)

    return K.mean(tf.concat([tf.divide(K.abs(diff), y_true_sliced), tf.divide(tf.square(P0 + Q0), tf.square(MTD))], 1))  # option 2