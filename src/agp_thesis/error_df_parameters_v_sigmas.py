import os
from .thesis_errors import *
from .create_id import create_id

error_options = {"mse" : mean_squared_error,
                "mape" : mean_absolute_percentage_error,
                "rmse" : root_mean_squared_error,
                "chisquare" : pearson_chi_square
                }


def error_df_parameters_v_sigmas(N, n_p, dt=None, treatment=True, uniform_dt=True, noise=True, it=0, gpu=False,
                                 noise_sigma=[0], activation='linear', errors=['mse'], 
                                 unique_id='', save_df=True, folder='New_Folder', cv_key=None, **others):
    '''Send all specifications used for the model that generated y_prime. Send all errors to calculate with y_true
    and y_prime. A df (Parameters vs sigmas) will be generated for every error.'''
    #
    # if cv_key is None:
    #     cv_key = [None]

    if not isinstance(cv_key, list):
        cv_key = [cv_key]

    list_of_e_df = []
    for ii in np.arange(len(noise_sigma)):
        for jj, key in enumerate(cv_key):
            current_sigma = noise_sigma[ii]
            # Build identifier name recursively
            identifier = create_id(N=N, n_p=n_p, dt=dt, treatment=treatment, uniform_dt=uniform_dt, noise=noise,
                                   current_sigma=current_sigma, it=it, gpu=gpu, cv_key=key)

            # try to load y_true and y_prime
            if key is None or key is '':
                ytrue_name ='y_true' + '_' + activation + identifier
                yprime_name = 'y_prime' + '_' + activation + identifier
            else:
                ytrue_name ='y_true' + '_' + activation + '_' + key + identifier
                yprime_name = 'y_prime' + '_' + activation + '_' + key + identifier

            y_test = pd.read_pickle(os.path.join(folder, ytrue_name))
            y_prime = pd.read_pickle(os.path.join(folder, yprime_name))

            print('\nElementwise Errors of the 8 parameters for sigma:', current_sigma)

            # General error calculation
            errors_per_sigma = [f(y_test, y_prime) for f in [error_options[x] for x in errors]]

            for jj, current_errors in enumerate(errors):
                # if ii == 0:
                #     # list_of_e_df.append(pd.DataFrame())
                #     list_of_e_df = errors_per_sigma
                e = errors_per_sigma[jj]
                e_columns = [s + '_' + current_errors for s in y_test.columns]
                E_CURRENT = pd.DataFrame(data=e.values, index=e_columns, columns=[current_sigma]).transpose()

                if ii == 0:
                    list_of_e_df.append(E_CURRENT)
                else:
                    list_of_e_df[jj] = list_of_e_df[jj].append(E_CURRENT)

    identifier = create_id(N=N, n_p=n_p, dt=dt, treatment=treatment, uniform_dt=uniform_dt, noise=noise,
                           current_sigma=current_sigma, it=it, gpu=gpu, add_sigma=False)
    e_name = '_' + activation + identifier + '_' + unique_id

    if save_df:
        for jj, current_errors in enumerate(errors):
            e_df = list_of_e_df[jj]
            print('Saving errors: ...', current_errors + e_name)
            e_df.to_pickle(os.path.join(folder, current_errors + e_name))

    return list_of_e_df
