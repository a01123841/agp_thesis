from sklearn.utils import check_array
import numpy as np
import pandas as pd


def mean_squared_error(y_true, y_pred):
    return pd.DataFrame(np.square(y_true - y_pred)).mean(axis=0)


def pearson_chi_square(y_true, y_pred):
    return pd.DataFrame(np.square(y_true - y_pred)/(y_true)).mean(axis=0)


def mean_square_percentage_error(y_true, y_pred):
    return pd.DataFrame(np.square(np.abs(y_true - y_pred) / np.square(y_true))).mean(axis=0)


def mean_absolute_percentage_error(y_true, y_pred):
    return pd.DataFrame(np.abs(y_true - y_pred)/y_true).mean(axis=0)


def root_mean_squared_error(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_true, y_pred))

