import numpy as np


def underlying_normal(mu, sigma):
    under_m = 2 * np.log(mu) - 1 / 2 * np.log(sigma ** 2 + mu ** 2)
    under_s = np.sqrt(-2 * np.log(mu) + np.log(sigma ** 2 + mu ** 2))
    return under_m, under_s


def sample_rand_parameter_logn(mu, CV, n=1):
    """ Random sampling from a log-normal distribution.

    Arguments:
    :param mu: Mean value (parameter estimate) in the Ribba paper.
    :param CV: Value in paraenthesis in the Ribba paper.
    :param n: number of samples

    Returns:
    :return parameter: the sample value

    """

    ## According to wikipedia:
    #     s_ln = sqrt(log(CV ^2+1));

    ## Calculation of SE
    SE = CV * mu / 100;  # rule of three

    ## Calculation of Sigma squared
    # v = SE ** 2;  # ...

    ## Transforming for numpy convention:

    # mu = np.log((m ** 2) / np.sqrt(v + m ** 2));
    # sigma = np.sqrt(np.log(v / (m ** 2) + 1));
    m, sigma = underlying_normal(mu, SE)


    ## Obtaining a parameter from the function handle
    parameter = np.random.lognormal(m, sigma, n)

    return parameter[0]
