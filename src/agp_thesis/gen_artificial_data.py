from .constants import *
from .samads_constants import *
from ._dynamical_model_parameters import model_parameters
from .plot_utils import group_and_scatter
from .ribbas_model_full import ribbas_model_full
from .samads_model import samads_model
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from tqdm import tqdm

from scipy.stats import truncnorm

#plt.style.use('ggplot')

# ERROR VALUES of mean distributions (parenthersis in paper):
# P0_pcv_me =          25  # [mm]
# Q0_pcv_me =          7  # [mm]
# lambda_p_pcv_me =    16 # [mo^-1]
# k_PQ_pcv_me =        21 # [mo^-1]
# k_Qp_P_pcv_me =      35 # [mo^-1]
# delta_Qp_pcv_me =    21 # [mo^-1]
# gamma_pcv_me =       37  #[]
# KDE_pcv_me =         33 # [mo^-1]
#
# PARAMETERS_PCV_ME = [P0_pcv_me, Q0_pcv_me, lambda_p_pcv_me, k_PQ_pcv_me, k_Qp_P_pcv_me, delta_Qp_pcv_me, gamma_pcv_me, KDE_pcv_me]

model_options = {'Ribbas': ribbas_model_full,
                 'Samads': samads_model
                 }
MTD_pcv_me = P0_pcv_me + Q0_pcv_me


def add_noise(mean=0, sigma=MTD_pcv_me, length=17, type='Normal'):
    """ Adding noise to curves MTD, P, and Q.

    Args:
        mean (float):
        sigma (float):
        length (int):
        type (str): Either 'Normal' or 'Lognormal'

    Returns:

    """
    noise_vector = []
    if type is 'Normal':
        noise_vector = np.random.normal(mean, sigma, length)
    elif type is 'Lognormal':
        noise_vector = np.random.normal(mean, sigma, length)

    return noise_vector


def _asserts(dt, n_p, fix_t0, fix_tf, plot_curves, ax, plot_title, parameters, model_name):
    if fix_t0 is None:
        fix_t0 = False
    if fix_tf is None:
        fix_tf = False
    if n_p is False:
        n_p = None
    if dt is False:
        dt = None

    if dt is not None and n_p is not None:
        raise ValueError('Distance between sampling points can be calculated with EITHER dt or n_p, but not both.')
    if dt is None and n_p is None:
        raise ValueError(
            'Distance between sampling points can be calculated with EITHER dt or n_p. Both cannot be None.')
    if dt and n_p:
        raise ValueError(
            'Distance between sampling points can be calculated with EITHER dt or n_p. Both cannot be True.')
    if not dt and not n_p:
        raise ValueError(
            'Distance between sampling points can be calculated with EITHER dt or n_p. Both cannot be False.')
    if isinstance(n_p, int):
        if dt or dt is not None:
            raise ValueError('n_p has a value. dt should be False or None.')

    if parameters is not None and not isinstance(parameters, pd.DataFrame):
        raise ValueError('parameters should be None or DataFrame.')

    if plot_curves and ax is None:
        fig, ax = plt.subplots()
        ax.set_title(plot_title)
        ax.set_xlabel("t")
        ax.axvline(x=0, lw=.5)
        if model_name == 'Samads':
            ax.set_ylim(0, 2)
            ax.set_xlim(-t0_mu_sam, tf_mu_sam)
            ax.set_ylabel('Response')
        else:
            ax.set_ylim(0, 110)
            ax.set_xlim(-60, 60)
            ax.set_ylabel("MTD = P+Q+Q_p")

    else:
        ax = None

    return fix_t0, fix_tf, ax, n_p, dt


def boundary_times(lower_t, upper_t, fix_t0, fix_tf, model_name):
    """ Get initial/final times.
    """
    t1 = []
    t2 = []

    if model_name == 'Samads':
        t1 = []
        t2 = []
        if fix_t0 is False:
            # t1 = -1 * np.random.lognormal(t0_under_m_sam, t0_under_s_sam, 1)
            t1 = -1 * np.abs(np.random.normal(t0_mu_sam, t0_sigma_sam, 1))
        else:
            t1 = fix_t0
        if fix_tf is False:
            # t2 = np.random.lognormal(tf_under_m_sam, tf_under_s_sam, 1)
            t2 = np.abs(np.random.normal(tf_mu_sam, tf_sigma_sam, 1))
            if upper_t is not None:
                if t2 > upper_t:
                    t2 = upper_t
        else:
            t2 = fix_tf
    else:
        if fix_t0 is False:
            # t1 = -1 * np.random.lognormal(t0_mu, t0_sigma, 1)
            t1 = -1 * np.random.lognormal(t0_under_m, t0_under_s, 1)
            if lower_t is not None:
                if t1 < lower_t:
                    t1 = lower_t
        else:
            t1 = fix_t0
        if fix_tf is False:
            # t2 = np.random.lognormal(tf_mu, tf_sigma, 1)
            t2 = np.random.lognormal(tf_under_m, tf_under_s, 1)
            if upper_t is not None:
                if t2 > upper_t:
                    t2 = upper_t
        else:
            t2 = fix_tf

    return t1, t2


def _sample_with_np(n_p, d_rows, d_rows_sigma, CURVES, uniform_dt):
    """ Sample from curve when n_p is sent and dt is free.
    """
    if uniform_dt:
        SAMPLE = CURVES.copy().iloc[::d_rows, :][0:n_p]
    else:
        while True:
            try:
                rows = np.absolute(np.round(np.random.normal(d_rows, d_rows_sigma, n_p)))
                rows = np.append([0], rows[:-1])
                SAMPLE = CURVES.copy().iloc[np.cumsum(rows), :]
                if len(SAMPLE) == n_p:
                    break
            except IndexError:
                d_rows_sigma = int(d_rows_sigma * .7)
                pass
    return SAMPLE


def _sample_with_dt(d_rows, d_rows_sigma, CURVES, uniform_dt):
    """ Sample from curve when dt is sent and n_p is free.
    """
    if uniform_dt:
        SAMPLE = CURVES.iloc[::d_rows, :]
    else:
        while True:
            try:
                rows = np.absolute(np.round(np.random.normal(d_rows, d_rows_sigma, 50)))
                rows = np.cumsum(np.append([0], rows[:-1]))
                rows = rows[rows < len(CURVES)]
                SAMPLE = CURVES.iloc[rows, :]
                break
            except IndexError:
                pass
    return SAMPLE


def sample_from_curve(n_p, dt, t1, t2, uniform_dt, CURVES, d_rows_sigma, max_length=30, del_t=.05):
    """
    Sample points from curve to get the curve of an artificial patient.
    If uniform_dt is False, add noise horizontally by randomizing d_rows:
    Create a vector of size n_p, sampled from a  Gaussian with mean d_rows and sigma = dt_sigma
        d_rows = []
        SAMPLES = []
    d_rows_sigma = dt_sigma/.05

    Args:
        n_p (int, boolean, None): If int or True, dt must be False or None.
        dt (int, boolean, None): If int or True, n_p must be False or None.
        t1 (float): Initial time.
        t2 (float): Final time.
        uniform_dt (bool): Regularly/irregularly sampled.
        CURVES (iterable): Full curve obtained when solving the D.E.
        d_rows_sigma (float): When sampling irregularly, this represents the sigma of the normal where the d_rows is sampled.
        max_length (int): Upper limit of number of points in series when n_p is free

    Returns:

    """

    if d_rows_sigma is None or d_rows_sigma is False:
        d_rows_sigma = int((4/del_t)/5)

    if n_p is True:
        while True:
            n_p = int(np.random.lognormal(n_p_under_m, n_p_under_s, 1))  # Truncate here, maximum of 30?
            if n_p < max_length:
                break
        dt = np.floor(t2 - t1) / n_p # THIS SHOULD BE n_p - 1 !!!!!
        d_rows = int(np.round(dt / CURVES.delta_t.iloc[0]))
        SAMPLES = _sample_with_np(n_p, d_rows, d_rows_sigma, CURVES, uniform_dt)
    elif dt is True:
        #         dt = sample_rand_parameter_logn(dt_mu, dt_sigma, 1)
        dt = np.random.lognormal(dt_under_m, dt_under_s)
        d_rows = int(np.round(dt / CURVES.delta_t.iloc[0]))
        SAMPLES = _sample_with_dt(d_rows, d_rows_sigma, CURVES, uniform_dt)
    elif isinstance(n_p, int):
        d_rows = int(np.floor(len(CURVES) / n_p))
        SAMPLES = _sample_with_np(n_p, d_rows, d_rows_sigma, CURVES, uniform_dt)
    elif isinstance(dt, (int, float)):
        d_rows = int(np.floor(dt / CURVES.delta_t.iloc[0]))
        SAMPLES = _sample_with_dt(d_rows, d_rows_sigma, CURVES, uniform_dt)
    return SAMPLES


def gen_artificial_data(N=10, n_p=None, dt=5, treatment=True, return_curves=True, uniform_dt=True,
                        noise=True, noise_type='Normal', noise_sigma=5, lower_t=-200, upper_t=200,
                        fix_t0=False, fix_tf=False, plot_curves=True, plot_samples=True, plot_base=True,
                        add_base=True, plot_title="Curves with different parameters", ax=None, parameters=None,
                        model_name='Ribbas', start_ID=1,
                        d_rows_sigma=None,
                        max_length=30,
                        multi_treat=False,
                        del_t=.05,
                        free_n_treat=False,
                        **others):
    # TODO: add bound_MTD = False
    #       add bound_param = False
    #       sigma_noise = ____, baseline = True,
    # default dt = 5 cause of the range of time means (-37 to 60)
    #     divided by average number of points per time series (18)
    # dt = 5 means sampling every 100th point
    # If dt or n_p are None, they will be sampled individually for every curve.
    ## Curves with differents parameters (sampled in-function):

    model = model_options[model_name]

    fix_t0, fix_tf, ax, n_p, dt = _asserts(dt, n_p, fix_t0, fix_tf, plot_curves, ax, plot_title, parameters, model_name)

    # t0, tf, x_label, y_label, del_t, xmin, xmax, ymin, ymax, title = model_parameters(model_name)
    t0, tf, x_label, y_label, _, xmin, xmax, ymin, ymax, title = model_parameters(model_name)

    # Initialize DataFrames with the mean values
    mean_parameters, mean_patient = model(t0, tf, ID=0, del_t=del_t, ax=ax, baseline=True, plot=False,
                                          treatment=treatment)
    art_parameters = mean_parameters
    art_samples = mean_patient
    art_samples_noised = art_samples
    art_curves = []
    if return_curves:
        art_curves = mean_patient

    lower_n_treat = 2
    upper_n_treat = 15
    mu_n_treat = 9
    sigma_n_treat = 4
    n_treats = 2

    for i in tqdm(np.arange(start_ID, start_ID + N)):
        # Sample here t1, t2 from a distribution to send to the function:
        t1, t2 = boundary_times(lower_t, upper_t, fix_t0, fix_tf, model_name)

        if free_n_treat:
            n_treats = int(truncnorm.rvs((lower_n_treat - mu_n_treat) / sigma_n_treat,
                                         (upper_n_treat - mu_n_treat) / sigma_n_treat, loc=mu_n_treat,
                                         scale=sigma_n_treat, size=1)[0])
        # print(n_treats)

        # GENERATE ARTIFICIAL CURVE WITH PARAMETERS:
        # if parameters is None:
        #     PARAMETERS, CURVES = model(t1, t2, ID=i, ax=ax, plot=plot_curves, treatment=treatment)
        # else:
        PARAMETERS, CURVES = model(t1, t2, ID=i, ax=ax, plot=plot_curves, treatment=treatment, parameters=parameters,
                                   multi_treat=multi_treat, del_t=del_t, n_treats=n_treats)
        #         CURVES = CURVES.loc[~CURVES.MTD.isnull(), :]

        if return_curves:
            # Concatenate full curves to art_curves
            art_curves = pd.concat([art_curves, CURVES])
            if dt is not None and art_curves.delta_t.iloc[0] > dt:
                raise ValueError(
                    'Dt used for sampling points from artificial curve cannot be smaller than time between points.')

        # Concatenate parameters to art_parameters
        art_parameters = pd.concat([art_parameters, PARAMETERS])

        SAMPLE = sample_from_curve(n_p, dt, t1, t2, uniform_dt, CURVES, d_rows_sigma, max_length, del_t)

        #         if multi_treat:
        #             second_dose_t = max(CURVES.loc[CURVES.t >= 0, :].loc[CURVES.C == 1, 't'])
        #             # print(second_dose_t)
        #             index1 = abs(SAMPLE.t).idxmin()
        #             index2 = abs(SAMPLE.t - second_dose_t).idxmin()
        #             # index = SAMPLE.loc[SAMPLE.t == sample_time, :].index.values[0]
        #             SAMPLE.loc[[index1, index2], 'C'] = 1

        #             # SAMPLE.loc[index2, 'C'] = 1
        #             # position_2 = SAMPLE.loc[SAMPLE.t>0, :].loc[SAMPLE.MTD == max(SAMPLE.MTD), :]
        n_treat_real = int(CURVES.loc[CURVES.C == 1, "C"].sum())
        #         print('true n', n_treat_real)
        if multi_treat:
            # print(second_dose_t)
            index1 = abs(SAMPLE.t).idxmin()
            SAMPLE.loc[index1, 'C'] = 1
            for jj in np.arange(n_treat_real - 1):
                #                 print(CURVES.loc[CURVES.t >= 0, :].loc[CURVES.C == 1, 't'])
                try:
                    nth_dose_t = CURVES.loc[CURVES.t >= 0, :].loc[CURVES.C == 1, 't'].values[jj + 1]
                    indexn = abs(SAMPLE.t - nth_dose_t).idxmin()
                    # index = SAMPLE.loc[SAMPLE.t == sample_time, :].index.values[0]
                    SAMPLE.loc[indexn, 'C'] = 1

                except:
                    pass
                    #                 print('index', indexn)
                    #             print('true n:', n_treat_real)
                    #             print('n treats in sample:', int(SAMPLE.loc[SAMPLE.C==1, "C"].sum()))
                    # SAMPLE.loc[index2, 'C'] = 1
                    # position_2 = SAMPLE.loc[SAMPLE.t>0, :].loc[SAMPLE.MTD == max(SAMPLE.MTD), :]

        else:
            index1 = abs(SAMPLE.t).idxmin()
            SAMPLE.loc[index1, 'C'] = 1
        # Add noise to points, vertically:
        if noise:
            SAMPLE_NOISED = SAMPLE.copy()
            noised = add_noise(sigma=noise_sigma, type=noise_type, length=len(SAMPLE))
            SAMPLE_NOISED.loc[:, y_label] = SAMPLE_NOISED.loc[:, y_label] + noised

            art_samples_noised = pd.concat([art_samples_noised, SAMPLE_NOISED])

        # Concatenate samples points to art_samples
        art_samples = pd.concat([art_samples, SAMPLE])

    # art_samples = art_samples.dropna()  # dropna or coerce
    if not add_base:
        art_samples = art_samples.loc[art_samples.ID != 0]

    if plot_curves:
        if plot_base:
            ignore = model(xmin, xmax, ID=0, ax=ax, baseline=True, plot=True, treatment=treatment)
        if treatment:
            ax.axvline(x=0, color='k', lw=.5)
        plt.show()

    if plot_samples:
        ignore = group_and_scatter(art_samples, x_label=x_label, y_label=y_label, title=plot_title,
                                   scatter=True, ax=None, treatment=treatment, plot_baseline=plot_base,
                                   model_name=model_name, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)

    if not treatment and model_name == 'Ribbas':
        art_parameters = art_parameters.drop(['k_Qp_P', 'delta_Qp', 'gamma', 'KDE'], axis=1)

    if return_curves:
        return art_parameters, art_samples, art_samples_noised, art_curves
    else:
        return art_parameters, art_samples, art_samples_noised, []
