from keras.callbacks import TensorBoard, Callback, ModelCheckpoint, EarlyStopping
import os


# Extend class through inheritance.
class TestCallback(Callback):
    def __init__(self, test_data):
        self.test_data = test_data

    def on_epoch_end(self, epoch, logs={}):
        x, y = self.test_data
        loss = self.model.evaluate(x, y, verbose=0)
        print('\nTesting loss: {}\n'.format(loss))


def callbacks_val_data(folder_path, tb_cb, validation_split, chpt_cb_last,
                       identifier_stamp, X_test, y_test, early_stop=False, patience=25):
    graph_identifier = 'Graph' + identifier_stamp
    callbacks = []
    if tb_cb:
        log_dir = os.path.join(folder_path, graph_identifier)
        tbCallback = TensorBoard(log_dir=log_dir, histogram_freq=0,
                                                 write_graph=True, write_images=True)
        callbacks.append(tbCallback)

    if validation_split is None:
        validation_data = (X_test, y_test)
    else:
        validation_data = None
        callbacks.append(TestCallback((X_test, y_test)))

    if chpt_cb_last:
        # checkpoint, only mantain last model
        chpt_fn = os.path.join(folder_path, "weights.best.hdf5")
    else:
        # checkpoint, everytme it improves
        chpt_fn = os.path.join(folder_path, "weights-improvement-{epoch:02d}-{val_loss:.2f}.hdf5")
    checkpoint = ModelCheckpoint(chpt_fn, monitor='val_loss', verbose=1, save_best_only=True, mode='min')
    callbacks.append(checkpoint)
    if early_stop:
        early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=patience, verbose=0, mode='auto')
        callbacks.append(early_stop)
    return callbacks, validation_data
