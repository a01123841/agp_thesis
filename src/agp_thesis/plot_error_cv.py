import numpy as np
import matplotlib.pyplot as plt


def plot_error_cv(dfs, title, ax=None, plot_all=True, color=['k'], scale=False, main_legend=['Norm total'],
                  regular_ticks=None, norm_function=None, vertical_ticks=True, linestyle='-', styles=None, lw=3):

    if norm_function is None:
        norm_function = lambda x: x.sum(axis=1) / np.sum(x.sum(axis=1))

    if not isinstance(dfs, list):
        dfs = [dfs]
    if not isinstance(color, list):
        color = [color]
    if not isinstance(main_legend, list):
        main_legend = [main_legend]

    for index, df in enumerate(dfs):
        if regular_ticks:
            tick_labels = df.index
            df = df.reset_index(drop=True)

        if scale:
            df = df.apply(lambda x: x / np.sum(x), axis=0)

        if ax is None:
            fig, ax = plt.subplots()

        if plot_all:
            df.iloc[:, 0:4].apply(lambda x: x.plot(ax=ax))
            df.iloc[:, 4:8].apply(lambda x: x.plot(ax=ax, linestyle='--'))
            norm_MSE = norm_function(df)
            norm_MSE.plot(color=color[index], ax=ax, linewidth=lw, title=title, linestyle=linestyle)
            legends = np.append(df.columns[0:8].values, main_legend)
        else:
            norm_MSE = norm_function(df)
            norm_MSE.plot(color=color[index], ax=ax, linewidth=lw, title=title, linestyle=linestyle)
            legends = main_legend

    ax.legend(legends)
    ax.xaxis.set_ticks(df.index.values)

    if regular_ticks is None or regular_ticks is False:
        pass
    else:
        if vertical_ticks:
            for i in df.index.values:
                ax.axvline(x=i, color='k', linestyle='--', alpha=.2)
        ax.set_xticklabels(tick_labels)

    return ax
