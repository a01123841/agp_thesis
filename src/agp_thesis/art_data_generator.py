from .constants import *
from .samads_constants import *
from ._dynamical_model_parameters import model_parameters
from .ribbas_model_full import ribbas_model_full
from .samads_model import samads_model
from .gen_artificial_data import boundary_times, sample_from_curve, add_noise
from tensorflow import Session, reshape
import numpy as np
import pandas as pd

model_options = {'Ribbas': ribbas_model_full,
                 'Samads': samads_model
                 }


def _asserts(dt, n_p, fix_t0, fix_tf, parameters, model_name):
    if fix_t0 is None:
        fix_t0 = False
    if fix_tf is None:
        fix_tf = False
    if n_p is False:
        n_p = None
    if dt is False:
        dt = None

    if dt is not None and n_p is not None:
        raise ValueError('Distance between sampling points can be calculated with EITHER dt or n_p, but not both.')
    if dt is None and n_p is None:
        raise ValueError(
            'Distance between sampling points can be calculated with EITHER dt or n_p. Both cannot be None.')
    if dt and n_p:
        raise ValueError(
            'Distance between sampling points can be calculated with EITHER dt or n_p. Both cannot be True.')
    if not dt and not n_p:
        raise ValueError(
            'Distance between sampling points can be calculated with EITHER dt or n_p. Both cannot be False.')
    if isinstance(n_p, int):
        if dt or dt is not None:
            raise ValueError('n_p has a value. dt should be False or None.')

    if parameters is not None and not isinstance(parameters, pd.DataFrame):
        raise ValueError('parameters should be None or DataFrame.')

    return fix_t0, fix_tf, n_p, dt


def art_data_generator(n_p=None, dt=5, treatment=True, uniform_dt=True,
                       noise=True, noise_type='Normal', noise_sigma=5,
                       lower_t=-200, upper_t=200, fix_t0=False, fix_tf=False, parameters=None,
                       model_name='Ribbas', train_columns=['MTD', 't', 'C'], objectives=None,
                       reshape3d=True,
                       d_rows_sigma=None,
                       **others):
    # TODO: add bound_MTD = False
    #       add bound_param = False
    #       sigma_noise = ____, baseline = True,
    # default dt = 5 cause of the range of time means (-37 to 60)
    #     divided by average number of points per time series (18)
    # dt = 5 means sampling every 100th point
    # If dt or n_p are None, they will be sampled individually for every curve.
    ## Curves with differents parameters (sampled in-function):

    model = model_options[model_name]

    fix_t0, fix_tf, n_p, dt = _asserts(dt, n_p, fix_t0, fix_tf, parameters, model_name)
    t0, tf, x_label, y_label, del_t, xmin, xmax, ymin, ymax, title = model_parameters(model_name)

    while True:
        # Sample here t1, t2 from a distribution to send to the function:
        t1, t2 = boundary_times(lower_t, upper_t, fix_t0, fix_tf, model_name)

        # GENERATE ARTIFICIAL CURVE WITH PARAMETERS:
        if parameters is None:
            PARAMETERS, CURVES = model(t1, t2, ID=1, plot=False, treatment=treatment)
        else:
            PARAMETERS, CURVES = model(t1, t2, ID=1, plot=False, treatment=treatment, parameters=None)

        SAMPLES = sample_from_curve(n_p, dt, t1, t2, uniform_dt, CURVES, d_rows_sigma)

        if not treatment and model_name == 'Ribbas':
            PARAMETERS = PARAMETERS.drop(['k_Qp_P', 'delta_Qp', 'gamma', 'KDE'], axis=1)
        SAMPLES = SAMPLES.dropna()  # dropna or coerce
        # Add noise to points, vertically:


        if train_columns is not None:
            if any([col == 'delta_t' for col in train_columns]) or any([col == '1_dt' for col in train_columns]):
                SAMPLES['delta_t'] = SAMPLES.groupby('ID')['t'].transform(lambda x: x.diff())
                SAMPLES = SAMPLES.fillna(0)
                SAMPLES['1_dt'] = SAMPLES.groupby('ID')['delta_t'].transform(lambda x: 1 / x)
                SAMPLES = SAMPLES.replace(np.inf, 0)

            SAMPLES = SAMPLES.loc[:, train_columns]

        if objectives is not None:
            objectives.append('ID')
            PARAMETERS = PARAMETERS.loc[:, objectives]

        if noise:
            SAMPLES_NOISED = SAMPLES.copy()
            noised = _add_noise(sigma=noise_sigma, type=noise_type, length=len(SAMPLES))
            SAMPLES_NOISED.loc[:, y_label] = SAMPLES_NOISED.loc[:, y_label] + noised
            SAMPLES = SAMPLES_NOISED

        if reshape3d:
            SAMPLES = reshape(np.array(SAMPLES), [1, SAMPLES.shape[0], SAMPLES.shape[1]])
            sess = Session()
            with sess.as_default():
                SAMPLES = SAMPLES.eval()
            PARAMETERS.pop('ID')

        yield (SAMPLES, np.array(PARAMETERS))
