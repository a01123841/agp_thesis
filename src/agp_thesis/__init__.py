# -*- coding: utf-8 -*-
import pkg_resources
import sklearn as sk
import pandas as pd
import sys
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
# import tensorflow as tf
import keras
from keras import backend as K
from keras.preprocessing.sequence import pad_sequences
import os.path

from scipy.stats import lognorm
from scipy.integrate import odeint

from keras.callbacks import Callback, ModelCheckpoint


"""
Constructor to initialize library and its modules.
"""

from .ribbas_model_full import ribbas_model_full
from .constants import *
from .sample_rand_parameter_logn import sample_rand_parameter_logn
from .fit_plot_lognormal import fit_plot_lognormal
from .plot_utils import *

from .thesis_errors import *
from .utilities import *

from .create_model import create_model
from .plot_error_cv import plot_error_cv
from .error_df_parameters_v_sigmas import error_df_parameters_v_sigmas
from .create_id import create_id
from .error_cubes import error_cubes
from .label_load_save_artificial_data import label_load_save_artificial_data
from .mspe_loss import mspe_loss
from .general_cv_errors import general_cv_errors
from .label_load_save_predictions import label_load_save_predictions
from .generate_predictions import generate_predictions
from .reshape_scale_split_data import reshape_scale_split_data, reshape_3d, pop_all_first, unshape_3d
from .calculate_fit_error import calculate_fit_error, fit_error
from ._columns_and_objectives import columns_and_objectives
from .sample_rand_parameter_logn import sample_rand_parameter_logn, underlying_normal

from .samads_constants import *
from .samads_model import samads_model
from .gen_artificial_data import gen_artificial_data, add_noise
from ._dynamical_model_parameters import model_parameters
from .unify_save_load_data import unify_save_load_data
from .join_clean_data import join_clean_data
from .art_data_generator import art_data_generator
from .callbacks_val_data import callbacks_val_data

from .test_data_generator import test_data_generator
from .assess_model import assess_model, constraint_for_P0, first_MTD_RMSE
from .assess_plot_losses import assess_plot_models
from .minmax_scaler_3d import minmax_scaler_3d
from .real_data_assess_and_predict import real_data_assess_and_predict, preprocess_real_data
from .postprocess_samples import postprocess_samples_for_monolix, correct_treatment_column, correct_zeros_C

from .mape_constrained_loss import mape_constrained_loss
from .mape_constrained_loss import mape_constrained_loss_no_treat

from .add_noise_samples import add_noise_samples

from .regression_rmse import regression_rmse

#plt.style.use('ggplot')

try:
    __version__ = pkg_resources.get_distribution(__name__).version
except:
    __version__ = '1.0'
    __author__ = "Andres Gonzalez"
    __copyright__ = "Andres Gonzalez"
    __license__ = "none"

