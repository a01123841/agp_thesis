import numpy as np
import pandas as pd
import os
from .error_df_parameters_v_sigmas import error_df_parameters_v_sigmas
from .create_id import create_id


def error_cubes(generation_kw, noise_sigma=[0], activations=['linear'], errors=['mse'],
                unique_id='', save_df=True, it=0, gpu=False, folder='New_Folder', cv_key = None):
    """

    :param generation_kw:
    :param noise_sigma:
    :param activations:
    :param errors:
    :param unique_id:
    :param save_df:
    :param it:
    :param gpu:
    :param folder:
    :param cv_key:
    :return:
    """

    list_of_list_of_e = []
    identifier = []
    for jj in np.arange(len(activations)):

        try:
            # Load error measures vs sigma for several activations, and group them by error type.
            identifier = create_id(**generation_kw, it=it, gpu=gpu, add_sigma=False)
            e_name = '_' + activations[jj] + identifier + '_' + unique_id

            print('Loading errors with ID:', e_name)
            for kk, current_error_name in enumerate(errors):
                CURRENT_DF = pd.read_pickle(os.path.join(folder, current_error_name + e_name))
                if jj == 0:
                    list_of_list_of_e.append([CURRENT_DF])
                else:
                    list_of_list_of_e[kk].append(CURRENT_DF)
        except:
            # Generate
            e_name = '_' + activations[jj] + identifier + '_' + unique_id
            print('Error df with name:', e_name, ' does not exist, attempting to create.')
            list_e_per_activation = error_df_parameters_v_sigmas(**generation_kw,
                                                                 noise_sigma=noise_sigma,
                                                                 activation=activations[jj],
                                                                 errors=errors,
                                                                 unique_id=unique_id,
                                                                 save_df=save_df, folder=folder, cv_key=cv_key)
            # Load error measures vs sigma for several activations, and group them by error type.
            identifier = create_id(**generation_kw, it=it, gpu=gpu, add_sigma=False)
            for kk, current_error_name in enumerate(errors):
                CURRENT_DF = pd.read_pickle(os.path.join(folder,current_error_name + e_name))
                if jj == 0:
                    list_of_list_of_e.append([CURRENT_DF])
                else:
                    list_of_list_of_e[kk].append(CURRENT_DF)

    if len(activations) == 1:
        print('Only one activation sent, returning slice for activation:', activations[0])

    if len(noise_sigma) == 1:
        print('Only one sigma sent, returning slice for sigma:', activations[0])

    list_of_cubes = []
    for kk in np.arange(len(errors)):
        list_of_cubes.append(pd.concat(list_of_list_of_e[kk], axis=0, keys=activations))

    return list_of_cubes
    #     # List of rmse (for one error sigma) to df and bar plot
    #     rmse_all_activations = pd.concat(list_of_rmse)
    #     rmse_all_activations.index = activations
    #     rmse_all_activations = rmse_all_activations.transpose()