from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, ELU, Masking, BatchNormalization
from keras.layers.advanced_activations import LeakyReLU, PReLU, ThresholdedReLU
from keras.regularizers import l1, l2, l1_l2
import numpy as np


activation_options = {"linear": "linear",
                    "relu": "relu",
                    "elu": ELU(),
                    "prelu": PReLU(alpha_initializer='zeros', alpha_regularizer=None, alpha_constraint=None, shared_axes=None),
                    "leaky_relu": LeakyReLU(alpha=.2),
                    "trelu": ThresholdedReLU()
                    }

# add reg options
# try combinations of l2 and l1
# l2 and dropout
# l1 alone

reg_options = { 'l1': l1(0.01),
                'l2': l2(0.01),
                'l1_l2': l1_l2(.01, .01),
                None: None
                }


def create_model(input_shape, drop_rate=.5, activation_name='linear', n_class=8, hidden_units=500,
                 loss='mse', multilayer=False, compress=False, dropout=True, b_reg=None, k_reg=None,
                 var_length=0,
                 mask_value=-100,
                 batch_norm=False):
    """Function to create model, required for KerasRegressor
    """
    print('Creating model... w. input shape ', input_shape, 'n classes', n_class, ' hidden units ', hidden_units)
    if compress:
        hidden_units2 = int(np.floor(hidden_units/3))
    else:
        hidden_units2 = hidden_units

    model_regression = Sequential()

    if var_length == 2:
        print('Variable length selected, masking values.')
        model_regression.add(Masking(mask_value=mask_value, input_shape=input_shape))

    if batch_norm:
        model_regression.add(BatchNormalization(input_shape=input_shape))

    if multilayer is False:
        model_regression.add(LSTM(hidden_units,
                                  input_shape=input_shape,
                                  kernel_initializer='TruncatedNormal',
                                  bias_initializer='TruncatedNormal',
                                  kernel_regularizer=reg_options[k_reg],
                                  bias_regularizer=reg_options[b_reg]))

    if multilayer is True or multilayer == 2:
        if batch_norm:
            model_regression.add(BatchNormalization())

        model_regression.add(LSTM(hidden_units,
                                  return_sequences=True,
                                  input_shape=input_shape,
                                  kernel_initializer='TruncatedNormal',
                                  bias_initializer='TruncatedNormal'))
        if dropout:
            model_regression.add(Dropout(drop_rate))

        if compress:
            model_regression.add(Dense(hidden_units2, kernel_initializer='TruncatedNormal',
                                       bias_initializer='TruncatedNormal'))

        if batch_norm:
            model_regression.add(BatchNormalization())
        model_regression.add(LSTM(hidden_units2,
                                  kernel_initializer='TruncatedNormal',
                                  bias_initializer='TruncatedNormal'))
    elif multilayer == 3:
        model_regression.add(LSTM(hidden_units,
                                  return_sequences=True,
                                  input_shape=input_shape,
                                  kernel_initializer='TruncatedNormal',
                                  bias_initializer='TruncatedNormal'))
        if dropout:
            model_regression.add(Dropout(drop_rate))

        if compress:
            model_regression.add(Dense(hidden_units2, kernel_initializer='TruncatedNormal',
                                       bias_initializer='TruncatedNormal'))
        model_regression.add(LSTM(hidden_units2,
                                  kernel_initializer='TruncatedNormal',
                                  bias_initializer='TruncatedNormal'))
        if dropout:
            model_regression.add(Dropout(drop_rate))
        model_regression.add(LSTM(hidden_units,
                                  kernel_initializer='TruncatedNormal',
                                  bias_initializer='TruncatedNormal'))

    # if batch_norm:
    #     model_regression.add(BatchNormalization())
    if dropout:
        model_regression.add(Dropout(drop_rate))
    activation = activation_options[activation_name]

    if activation is 'linear' or activation is 'relu':
        model_regression.add(Dense(n_class, activation=activation, kernel_initializer='TruncatedNormal',
                                   bias_initializer='TruncatedNormal'))
    # elif activation is 'leaky_relu':
    else:
        # activation = activation_options[activation_name]
        model_regression.add(Dense(n_class, kernel_initializer='TruncatedNormal',
                                   bias_initializer='TruncatedNormal'))
        model_regression.add(activation)
    model_regression.compile(loss=loss, optimizer='adam', metrics=['accuracy'])
    #     model_regression.summary()
    return model_regression
###############33
#
# import numpy as np
# np.random.seed(1)
#
# import tensorflow as tf
# tf.set_random_seed(1)
#
# from keras import models
# from keras.layers import Dense, Masking, LSTM
#
# import matplotlib.pyplot as plt
#
#
# def stateful_model():
#     hidden_units = 256
#
#     model = models.Sequential()
#     model.add(LSTM(hidden_units, batch_input_shape=(1, 1, 1), return_sequences=False, stateful=True))
#     model.add(Dense(1, activation='relu', name='output'))
#
#     model.compile(loss='binary_crossentropy', optimizer='rmsprop')
#
#     return model
#
#
# def train_rnn(x_train, y_train, max_len, mask):
#     epochs = 10
#     batch_size = 200
#
#     vec_dims = 1
#     hidden_units = 256
#     in_shape = (max_len, vec_dims)
#
#     model = models.Sequential()
#
#     model.add(Masking(mask, name="in_layer", input_shape=in_shape,))
#     model.add(LSTM(hidden_units, return_sequences=False))
#     model.add(Dense(1, activation='relu', name='output'))
#
#     model.compile(loss='binary_crossentropy', optimizer='rmsprop')
#
#     model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs,
#               validation_split=0.05)
#
#     return model
#
#
# def gen_train_sig_cls_pair(t_stops, num_examples, mask):
#     x = []
#     y = []
#     max_t = int(np.max(t_stops))
#
#     for t_stop in t_stops:
#         one_indices = np.random.choice(a=num_examples, size=num_examples // 2, replace=False)
#
#         sig = np.zeros((num_examples, max_t), dtype=np.int8)
#         sig[one_indices, 0] = 1
#         sig[:, t_stop:] = mask
#         x.append(sig)
#
#         cls = np.zeros(num_examples, dtype=np.bool)
#         cls[one_indices] = 1
#         y.append(cls)
#
#     return np.concatenate(x, axis=0), np.concatenate(y, axis=0)
#
#
# def gen_test_sig_cls_pair(t_stops, num_examples):
#     x = []
#     y = []
#
#     for t_stop in t_stops:
#         one_indices = np.random.choice(a=num_examples, size=num_examples // 2, replace=False)
#
#         sig = np.zeros((num_examples, t_stop), dtype=np.bool)
#         sig[one_indices, 0] = 1
#         x.extend(list(sig))
#
#         cls = np.zeros((num_examples, t_stop), dtype=np.bool)
#         cls[one_indices] = 1
#         y.extend(list(cls))
#
#     return x, y
#
#
# if __name__ == '__main__':
#     noise_mag = 0.01
#     mask_val = -10
#     signal_lengths = (10, 15, 20)
#
#     x_in, y_in = gen_train_sig_cls_pair(signal_lengths, 10, mask_val)
#
#     mod = train_rnn(x_in[:, :, None], y_in, int(np.max(signal_lengths)), mask_val)
#
#     testing_dat, expected = gen_test_sig_cls_pair(signal_lengths, 3)
#
#     state_mod = stateful_model()
#     state_mod.set_weights(mod.get_weights())
#
#     res = []
#     for s_i in range(len(testing_dat)):
#         seq_in = list(testing_dat[s_i])
#         seq_len = len(seq_in)
#
#         for t_i in range(seq_len):
#             res.extend(state_mod.predict(np.array([[[seq_in[t_i]]]])))
#
#         state_mod.reset_states()
#
#     fig, axes = plt.subplots(2)
#     axes[0].plot(np.concatenate(testing_dat), label="input")
#
#     axes[1].plot(res, "ro", label="result", alpha=0.2)
#     axes[1].plot(np.concatenate(expected, axis=0), "bo", label="expected", alpha=0.2)
#     axes[1].legend(bbox_to_anchor=(1.1, 1))
#
#     plt.show()