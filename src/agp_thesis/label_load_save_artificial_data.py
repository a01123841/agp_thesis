from .gen_artificial_data import gen_artificial_data
from .add_noise_samples import add_noise_samples
from .create_id import create_id
from .plot_utils import group_and_scatter
from ._dynamical_model_parameters import model_parameters
from .unify_save_load_data import unify_save_load_data
import numpy as np
import pandas as pd
import os


def label_load_save_artificial_data(generation_kw, generation_opts, plot_opts, N_per_file = 40000,
                                    current_sigma=0, gpu=False,
                                    folder='./Data/Pool', N_plot_preview=15,
                                    model_name='Ribbas', start_ID=1, identifier=None,
                                    unify=True,
                                    d_rows_sigma=None,
                                    max_length=30,
                                    multi_treat=False,
                                    del_t=.05,
                                    free_n_treat=False,
                                    # dt_sample=5,
                                    **others):

    t0, tf, x_label, y_label, _, xmin, xmax, ymin, ymax, title = model_parameters(model_name)

    # if current_sigma == 0:
    #     generation_kw['noise'] = False
    #
    # if current_sigma != 0:
    #     generation_kw['noise'] = True

    N = generation_kw['N']
    noise = generation_kw['noise']
    treatment = generation_kw['treatment']
    return_curves = generation_opts['return_curves']
    samples_noised = []
    generation_kw2 = generation_kw.copy()
    check = 0
    curves = []

    # base_identifier = create_id(**generation_kw, current_sigma=0, it=0, gpu=gpu, add_sigma=False,
    #                             model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)
    # try:
    #     base_identifier = base_identifier.replace("_noise1", "")
    # except:
    #     base_identifier = base_identifier.replace("_noise0", "")
    # first we check if final joint file exists
    if identifier is None:
        final_identifier = create_id(**generation_kw, current_sigma=current_sigma, it=0, gpu=gpu, add_sigma=False,
                                    model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)
        noise_final_identifier = []
        if noise:
            noise_final_identifier = create_id(**generation_kw, current_sigma=current_sigma, it=0, gpu=gpu, add_sigma=True,
                                     model_name=model_name, multi_treat=multi_treat, free_n_treat=free_n_treat)
    else:
        final_identifier = identifier
    fname_samples = os.path.join(folder, 'art_samples' + final_identifier)

    if os.path.isfile(fname_samples):
        print('Final file already exists, loading samples with identifier ', final_identifier)
        fname_parameters = os.path.join(folder, 'art_parameters' + final_identifier)

        if noise or current_sigma != 0:
            samples = pd.read_pickle(fname_samples)
            parameters = pd.read_pickle(fname_parameters)
            fname_samples_noised = os.path.join(folder, 'art_samples_noised' + noise_final_identifier)
            if os.path.isfile(fname_samples_noised):
                print('Final file for noisy samples exists, loading noised samples with identifier ', noise_final_identifier)
                samples_noised = pd.read_pickle(fname_samples_noised)
            else:
                print("Samples without noise exist. But samples with desired sigma ", current_sigma, " does not exist.")
                print("Attempting to create based on preexisting samples")
                samples_noised = add_noise_samples(samples, noise_sigma=current_sigma, plot_samples=True,
                                                     plot_title="Noisy Curves with different parameters",
                                                     model_name='Ribbas')
                samples_noised.to_pickle(fname_samples_noised)
        samples.head()
        parameters.head()
        print('Samples and Parameters shapes:', samples.shape, parameters.shape)

    else:
        if N / N_per_file > 1:
            times = int(np.ceil(N/N_per_file))
            generation_kw2['N'] = int(np.ceil(N/times))
            check = 1

        for it in np.arange(np.ceil(N / N_per_file)):
            # if identifier is None:
            identifier = create_id(**generation_kw, current_sigma=current_sigma, it=int(it+check), gpu=gpu,
                                   add_sigma=False, model_name=model_name, multi_treat=multi_treat,
                                   free_n_treat=free_n_treat)
            noise_identifier = create_id(**generation_kw, current_sigma=current_sigma, it=int(it + check), gpu=gpu,
                                   add_sigma=True, model_name=model_name, multi_treat=multi_treat,
                                   free_n_treat=free_n_treat)

            fname_parameters = os.path.join(folder, 'art_parameters' + identifier)
            fname_samples = os.path.join(folder, 'art_samples' + identifier)
            fname_samples_noised = os.path.join(folder, 'art_samples_noised' + noise_identifier)
            fname_curves = os.path.join(folder, 'art_curves' + identifier)

            # we check if the partial file existes
            if not os.path.isfile(fname_samples):
                print('Generating curves with identifier:', identifier)
                if it > 0:
                    plot_opts['add_base'] = False
                    start_ID = (N_per_file * it) + 1

                parameters, samples, samples_noised, curves = gen_artificial_data(**generation_kw2,
                                                                                  noise_sigma=current_sigma,
                                                                                  **generation_opts,
                                                                                  **plot_opts,
                                                                                  model_name=model_name,
                                                                                  start_ID=start_ID,
                                                                                  d_rows_sigma=d_rows_sigma,
                                                                                  max_length=max_length,
                                                                                  multi_treat=multi_treat,
                                                                                  del_t=del_t,
                                                                                  free_n_treat=free_n_treat)
                # print(noise, current_sigma, len(samples_noised))

                if noise or current_sigma != 0:
                    samples_noised.to_pickle(fname_samples_noised)
                if return_curves:
                    curves.to_pickle(fname_curves)
                samples.to_pickle(fname_samples)
                parameters.to_pickle(fname_parameters)
            else:
                print('File already exists, loading file', fname_samples)
                if noise or current_sigma != 0:
                    samples_noised = pd.read_pickle(fname_samples_noised)
                samples = pd.read_pickle(fname_samples)
                parameters = pd.read_pickle(fname_parameters)
                samples.head()
                parameters.head()
                print('Samples and Parameters shapes:', samples.shape, parameters.shape)

        if unify:
            parameters, samples, samples_noised, final_identifier = unify_save_load_data(generation_kw, generation_opts,
                                                                                         plot_opts,
                                                                                         N_per_file=N_per_file,
                                                                                         current_sigma=current_sigma,
                                                                                         gpu=gpu, folder=folder,
                                                                                         N_plot_preview=15,
                                                                                         model_name=model_name,
                                                                                         multi_treat=multi_treat,
                                                                                         free_n_treat=free_n_treat)

    # Plot first N series from the sampled data
    if noise or current_sigma != 0:
        ignore = group_and_scatter(samples_noised, x_label=x_label, y_label=y_label, title=title, scatter=False,
                                   ax=None, N=N_plot_preview, plot_baseline=False, treatment=treatment,
                                   model_name=model_name, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)
    ignore = group_and_scatter(samples, x_label=x_label, y_label=y_label, title=title, scatter=True, ax=None,
                               N=N_plot_preview, plot_baseline=False, treatment=treatment,
                               model_name=model_name, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)

    return parameters, samples, samples_noised, curves, final_identifier, noise_final_identifier
