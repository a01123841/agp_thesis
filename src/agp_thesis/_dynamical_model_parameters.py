from .constants import t0_m, tf_m
from .samads_constants import t0_mu_sam, tf_mu_sam


def model_parameters(model_name='Ribbas'):    
    if model_name == 'Samads':
        t0 = -t0_mu_sam
        tf = tf_mu_sam
        x_label = 't'
        y_label = 'Y'
        del_t = .3
        xmin = -t0_mu_sam*1.2
        xmax = tf_mu_sam
        ymin = -.25
        ymax = 1.25
        title = 'Response vs Time'
    else:
        t0 = t0_m
        tf = tf_m
        del_t = .05
        x_label = 't'
        y_label = 'MTD'
        xmin = t0_m*1.2
        xmax = tf_m*1.2
        ymin = 0
        ymax = 110
        title = 'MTD vs Time'
        
    return t0, tf, x_label, y_label, del_t, xmin, xmax, ymin, ymax, title
