from itertools import chain, combinations
from sklearn import linear_model
import numpy as np
import pandas as pd


def powerset(iterable):
    """
    powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)
    """
    xs = list(iterable)
    # note we return an iterator rather than a list
    return chain.from_iterable(combinations(xs, n) for n in range(len(xs)+1))


def lin_reg_interpolation(df, cv_column='N', error_column='mean_mape', extrapolate_values=[5, 1, 0]):
    """ Interpolation via linear regression. Fit a line to series of data.

    Args:
        df (DataFrame): Data  containing x and y.
        cv_column (str): Independent variable used for cross validation.
        error_column (str): Dependent variable, usually a type of error. i.e. 'mape'
        extrapolate_values (list): Values where we want to predict error through extrapolation.

    Returns:
        Dataframe: DataFrame with values obtained through regression.

    """
    x_fit = df.loc[:, error_column].values
    y_fit = np.log10(df.loc[:, cv_column].values)

    x_fit = x_fit.reshape(len(x_fit), 1)
    y_fit = y_fit.reshape(len(y_fit), 1)

    regr = linear_model.LinearRegression()
    regr.fit(x_fit, y_fit)

    error_to_predict = np.append(x_fit, extrapolate_values) # extrapolate error
    error_to_predict = error_to_predict.reshape(len(error_to_predict), 1)

    N_interp_error = np.power(10, regr.predict(error_to_predict))
    interp_data = np.array([N_interp_error, error_to_predict]).reshape(2, len(N_interp_error)).transpose()
    interpolation_df = pd.DataFrame(data=interp_data, columns=[cv_column + '_est', 'mean_mape'])

    return interpolation_df


def sample_by_nearest_time(true_samples, continuous):
    true_times = true_samples.t
    cont_pred_times = continuous.t

    indexes = []

    for i, true_t in enumerate(true_times):
        index = abs(cont_pred_times - true_t).idxmin()
        indexes.append(index)

    pred_patient = continuous.loc[indexes, :]
    return pred_patient
