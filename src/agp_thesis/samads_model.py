import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.integrate import odeint
from numpy.random import uniform
from .samads_constants import *
from ._dynamical_model_parameters import model_parameters

#plt.style.use('ggplot')

# ## ###########    SAMAD's PARAMETERS     #################################
# theta_i = 0
# theta_f = 500
#
# Kp_i = .5
# Kp_f = 1.5
#
# Tp_i = 10
# Tp_f = 200


def samads_model(t0, tf, ID, del_t=.3, plot=True, ax=None, baseline=False,
                parameters=None, **others):
    # [ID, P, Q, Q_P, MTD, C, t, delta_t]
    '''
    Generation of a time series following Samad's paper. Parameters are also
    sampled within the function.


    $$ \frac{dy}{dt} = - \frac{1}{\tau_p} y + \frac{K_p}{\tau_p } u(t - \theta)  $$

    Note: keep name of functions for data_generation descriptive.
    Q: How should I store data? Same as pcv data?
    Q: plot within function? (I think not)

    Arguments:
        t_0
        t_f

        plot_figure: potential

    Returns:

    '''
    # ###
    # plot_figure = True
    # treatment = False

    #
    #
    # baseline = False
    # parameters = None
    # t0 = t1
    # tf = t2
    # ID = 0
    # ax = None
    t0, tf, x_label, y_label, del_t, xmin, xmax, ymin, ymax, title = model_parameters('Samads')

    if baseline:
        Tp, Kp, theta = Tp_m, Kp_m, theta_m
    else:
        if parameters is None:
            ## Parameter generation
            num = 1
            Tp = uniform(Tp_i, Tp_f, num)[0]
            Kp = uniform(Kp_i, Kp_f, num)[0]
            theta = uniform(theta_i, theta_f, num)[0]
        else:
            ID, Tp, Kp, theta = parameters.loc[:, ['ID', 'Tp', 'Kp', 'theta']].values[0]

    parameters = np.array([ID, Tp, Kp, theta])

    # solve the system dy/dt = f(y, t)
    def f1(y, t):
        Y = y[0]
        U = np.heaviside(t - theta, 1)
        # dy/dt
        f0 = - 1 / Tp * Y + Kp / Tp * U

        return [f0]

    # initial conditions
    y0 = [0]  # initial condition vector
    t = np.arange(t0, tf, del_t)  # time grid

    # solve the DEs for pretreatment
    soln = odeint(f1, y0, t)

    # RETURNS:
    ID = ID * np.ones(len(t))
    Y = soln[:, 0]
    U = np.heaviside(t, 1)
    delta_t = np.diff(t)

    if plot:
        if ax is None:
            fig, ax = plt.subplots(1, 1)
            ax.set_title(title)
            ax.set_ylim(ymin, ymax)
            ax.set_xlim(xmin, xmax)
            ax.set_xlabel(x_label)
            ax.set_ylabel(y_label)
        if baseline:
            ax.plot(t, Y, color='k', lw=3)
            ax.plot(t, U, color='r', lw=3, alpha=.5)
        else:
            ax.plot(t, Y)

    curve_df = pd.DataFrame(data=[ID, Y, U, t, delta_t],
                            index=['ID', 'Y', 'U', 't', 'delta_t']).transpose()
    parameters_df = pd.DataFrame(data=parameters,
                                 index=['ID', 'Tp', 'Kp', 'theta']).transpose()

    return parameters_df, curve_df

# mean_parameters, mean_patient = ribbas_model_full(-37, 50, ID=0, ax=ax, baseline=True, plot=False, treatment=treatment)