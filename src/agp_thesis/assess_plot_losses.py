import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from .assess_model import assess_model
from tqdm import tqdm


# function takes folder
# Given a folder, iterate all hdf5 files.
# Get name: get last five characters; convert to float.
# strip 'weights_improvement_' and use the rest to append to epoch

# use file to asses a given test_data, save result
# plot val_loss and test_loss vs epochs; choose best test_loss
# print and  return epoch and recommended patience term


def plot_losses(losses_df, test_size, ax=None):
    if ax is None:
        fig, ax = plt.subplots()
    title = 'Comparison of Train (' + str(int(100 - test_size * 100)) + '%), Validation (' + str(
        int(test_size * 100)) + '%) and Test Losses during training, N=100k'
    try:
        ax = losses_df.plot(x='epochs', y=['train_loss'], color='g', title=title, ax=ax)
        ax = losses_df.plot(x='epochs', y=['val_loss'], ax=ax)

        ax = losses_df.plot.scatter(x='epochs', y='test_loss', ax=ax)

        ax.set_ylim([0, 50])
        ax.legend(['Train Loss', 'Val. Loss', 'Test Loss'])
        ax.set_ylabel('Loss')
        ax.set_xlabel('Epochs')
    except:
        pass

    return ax


def prepare_losses_df(history_df, epochs, val_loss, test_loss, rmse_loss, all_first):

    epochs = [int(x) for x in np.array(epochs).transpose()]
    test_loss = [float(x) * 100 for x in np.array(test_loss).transpose()]
    val_loss = [float(x) for x in np.array(val_loss).transpose()]

    losses_df = pd.DataFrame(data=np.array(test_loss).transpose(), columns=['test_loss'])
    if history_df is not None:
        losses_df['val_loss'] = pd.DataFrame(data=val_loss)
    try:
        losses_df['epochs'] = pd.DataFrame(data=epochs)
        losses_df.sort_values('epochs', inplace=True)
        losses_df.reset_index(drop=True, inplace=True)

        if history_df is not None:
            losses_df['train_loss'] = history_df.iloc[losses_df.epochs.values, :].loss.values

        losses_df['d_epochs'] = losses_df.epochs.transform(lambda x: x.diff())
        losses_df['d_test_loss'] = losses_df.test_loss.transform(lambda x: x.diff())

        if all_first is not None:
            losses_df['rmse_loss'] = pd.DataFrame(data=rmse_loss)
    except:
        pass

    return losses_df


def assess_plot_models(folder_path, X_test, y_test,
                       history_df,
                       drop_rate, activation,
                       hidden_units, loss,
                       layers, compress, error='mape',
                       test_size=.2,
                       plot=True,
                       var_length=0,
                       treatment=True,
                       scale_targets=False,
                       use_constraint=False,
                       all_first=None,
                       ax=None,
                       return_ax=False,
                       **others):
    """ Evaluate all models in a given path with the given dataset. Also plot history (train, val, test losses).


    Args:
        folder_path: Absolute path of where the models reside.
        X_test: Data in 3d shape (patients, len(series), columns)
        y_test: Targets
        history_df:
        input_shape:
        drop_rate:
        activation:
        hidden_units:
        loss:
        layers:
        compress:
        error:
        test_size:
        plot:
        var_length:
        treatment:
        scale_targets:
        use_constraint:
        all_first:

    Returns:

    """
    val_loss = []
    test_loss = []
    epochs = []
    best_model = []
    best_y_prime = []
    best_y_prime_unscaled = []
    rmse_loss = []
    minimum_error = 1000

    input_shape = (X_test.shape[1], X_test.shape[2])

    files = os.listdir(folder_path)
    # ordered_files = sorted(files, key=lambda x: (int(re.sub('\D', '', x)), x))
    files = sorted(files)  # did this work alphanumerically?
    print('There are a total of ', len(files), ' models saved.')
    for file in tqdm(files):
        # for file in files:
        if file.endswith(".hdf5"):
            sub_string = file.replace('weights-improvement-', '').replace('.hdf5', '')
            current_val_loss = sub_string[-5:]
            current_epoch = sub_string.replace(current_val_loss, '')
            current_val_loss = current_val_loss.replace('-', '')
            current_epoch = current_epoch.replace('-', '')
            # current_epoch = sub_string[:-6]

            epochs.append(current_epoch)
            val_loss.append(current_val_loss)
            print('Epoch: ', current_epoch, ' Val loss: ', current_val_loss)

            weights_fn = file
            print(weights_fn)

            model, y_prime, y_prime_unscaled, parameter_errors, rmse = \
                assess_model(folder_path, weights_fn,
                             X_test, y_test,
                             input_shape, drop_rate, activation,
                             hidden_units, loss,
                             layers, compress, error=error,
                             var_length=var_length,
                             treatment=treatment,
                             scale_targets=scale_targets,
                             use_constraint=use_constraint,
                             all_first=all_first)

            mean_test_loss = parameter_errors.mean()
            test_loss.append(mean_test_loss)

            if mean_test_loss < minimum_error:
                minimum_error = mean_test_loss
                best_model, best_y_prime, best_y_prime_unscaled = model, y_prime, y_prime_unscaled

            if all_first is not None:
                rmse_loss.append(rmse)

    losses_df = prepare_losses_df(history_df, epochs, val_loss, test_loss, rmse_loss, all_first)

    print(losses_df.head())
    if plot:
        ax = plot_losses(losses_df, test_size, ax=ax)

    if return_ax:
        return losses_df, best_model, best_y_prime, best_y_prime_unscaled, ax
    else:
        return losses_df, best_model, best_y_prime, best_y_prime_unscaled
