from .create_model import create_model
from .reshape_scale_split_data import reshape_3d
from .constants import *
import os


def constraint_for_P0(all_first, predicted_y):
    # If dataframe, reshape 3d
    print('Calculating fit of initial point for model, using constraing for P0')
    predicted_y['P0_c'] = all_first.drop('ID', axis=1).MTD.values - predicted_y['Q0'].values

    # Correction of negative values of PO_c
    predicted_y.loc[predicted_y['P0_c']<0, 'P0_c'] = 0.01

    predicted_y['MTD_c'] = predicted_y['P0_c'] + predicted_y['Q0']
    # print('Mean initial MTD of the real data: ', all_first.drop('ID', axis=1).MTD.mean())
    rmse = np.sqrt(mean_squared_error(all_first.drop('ID', axis=1).MTD.values, predicted_y.MTD_c.values))
    print('RMSE for first MTD versus first (predicted with constraint) MTD´´ :', rmse.values[0])
    return predicted_y, rmse.values[0]


def first_MTD_RMSE(all_first, predicted_y):
    print('Calculating fit of initial point for model.')
    predicted_y['MTD'] = predicted_y['P0'] + predicted_y['Q0']
    # print('Mean initial MTD of the real data: ', all_first.drop('ID', axis=1).MTD.mean())
    rmse = np.sqrt(mean_squared_error(all_first.drop('ID', axis=1).MTD.values, predicted_y.MTD.values))
    # print('RMSE for first MTD versus first (predicted) MTD´ :', rmse.values[0])
    print('RMSE for first MTD versus first (predicted) MTD´ :', rmse)
    return rmse.values[0]


def assess_model(folder_path, weights_fn, X_test, y_test,
                 input_shape, drop_rate, activation,
                 hidden_units, loss,
                 layers, compress, error='mape',
                 var_length=0, treatment=True,
                 scale_targets=False,
                 use_constraint=False, all_first=None,
                 batch_norm=False, pad_value=-100):
    """
    Note: Data is sent already in 3d array form, and padded, along a separate df containign first values of MTD

    This should be done after the curves are saved but before padding in the resape_... function.
    For the two subfunctions above, decide whether I send arrays only or dataframes only.

    Args:
        folder_path:
        weights_fn:
        X_test: Must be sent already reshaped in 3d.
        y_test: Can be None. If so, no errors are calcualted.
        input_shape:
        drop_rate:
        activation:
        hidden_units:
        loss:
        layers:
        compress:
        error:
        var_length:
        treatment:
        scale_targets:
        use_constraint:
        all_first:

    Returns:

    """
    if use_constraint and all_first is None:
        raise ValueError('When using constraint, all first MTD values must be sent.')

    rmse_first = []
    y_prime_unscaled =[]
    if treatment:
        parameter_columns = ['P0', 'Q0', 'lambda_p', 'k_PQ', 'k_Qp_P', 'delta_Qp', 'gamma', 'KDE']
    else:
        parameter_columns = ['P0', 'Q0', 'lambda_p', 'k_PQ']
    n_class = len(parameter_columns)
    error_function = error_options[error]

    # if isinstance(X_test, pd.DataFrame):
    #     X_test = reshape_3d(samples, var_length, train_columns, N, n_p, max_length, pad_value, use_constraint)

    best_model = create_model(input_shape, drop_rate, activation, n_class,
                              hidden_units, loss=loss,
                              multilayer=layers,
                              compress=compress,
                              var_length=var_length,
                              batch_norm=batch_norm,
                              mask_value=pad_value)

    print('Loading weights from ', folder_path)
    best_model.load_weights(os.path.join(folder_path, weights_fn))
    y_prime = best_model.predict(X_test)

    errors = []
    # If use_constraint, I have to select the correct y_prime
    if y_test is not None:
        errors = error_function(y_test, y_prime)
        print('Parameters errors: ')
        print(errors)
        print('Mean error: ', errors.mean())
    else:
        print('No y_test sent, using all 1s, errors ignored')
        y_test = np.random.choice([1], [len(y_prime), n_class])

    # Note: model was trained with scaled targets. We need to scale back
    if scale_targets:
        print('Scaling back the targets and returning them in df')
        y_prime_unscaled = np.asarray(
            [y_prime[:, i] * PARAMETERS_PCV_M[i] for i in np.arange(y_prime.shape[1])]).transpose()
        y_test_unscaled = np.asarray(
            [y_test[:, i] * PARAMETERS_PCV_M[i] for i in np.arange(y_test.shape[1])]).transpose()
        if error is not 'mape':
            errors = error_function(y_test_unscaled, y_prime_unscaled)
            print('Parameters error after scaling back: ', errors)
            print('Mean error after scaling back: ', errors.mean())
        # IDs = np.arange(y_prime.shape[0]) + 1
        # y_prime_unscaled = pd.DataFrame(data=np.c_[IDs, y_prime_unscaled], columns=parameter_columns)
        y_prime_unscaled = pd.DataFrame(data=y_prime_unscaled, columns=parameter_columns)

        if use_constraint:
            parameter_columns[0] = 'P0_c'
            y_prime_unscaled, rmse_first = constraint_for_P0(all_first, y_prime_unscaled)
            y_prime_c = y_prime_unscaled.loc[:, parameter_columns].values
            errors = error_function(y_test_unscaled, y_prime_c)
            print('CORRECTED Mean error: ', errors.mean())
            y_prime = y_prime_c
        elif all_first is not None:
            rmse_first = first_MTD_RMSE(all_first, y_prime_unscaled)
        y_prime = pd.DataFrame(data=y_prime, columns=parameter_columns)
    else:
        y_prime = pd.DataFrame(data=y_prime, columns=parameter_columns)
        # here y must be sent as df
        if use_constraint:
            parameter_columns[0] = 'P0_c'
            y_prime, rmse_first = constraint_for_P0(all_first, y_prime)
            y_prime_c = y_prime.loc[:, parameter_columns].values
            errors = error_function(y_test, y_prime_c)
            print('CORRECTED Mean error: ', errors.mean())
            y_prime = y_prime_c
        elif all_first is not None:
            rmse_first = first_MTD_RMSE(all_first, y_prime)

    return best_model, y_prime, y_prime_unscaled, errors, rmse_first
