import numpy as np
from .utilities import sample_by_nearest_time
from .thesis_errors import mean_squared_error


# Calculation of RMSE for samples and extended samples
def regression_rmse(true_samples, pred_samples, true_extension, fine_sampling=False,
                    list_rmse=None, list_rmse_ext=None,
                    list_true_extension=None, list_pred_extension=None):

    if list_rmse is None:
        list_rmse = []
    if list_rmse_ext is None:
        list_rmse_ext = []

    if list_true_extension is None:
        list_true_extension = []
    if list_pred_extension is None:
        list_pred_extension = []

    IDs = true_samples.ID.unique()

    true_groups = true_samples.groupby('ID')
    pred_groups = pred_samples.groupby('ID')

    if true_extension is not None:
        ext_groups = true_extension.groupby('ID')

    for i, ID in enumerate(IDs):
        true_patient = true_groups.get_group(ID)
        n_samples = len(true_patient)
        if not fine_sampling:
            pred_patient = pred_groups.get_group(ID).iloc[0:n_samples, :]
        else:
            pred_patient = sample_by_nearest_time(true_patient, pred_groups.get_group(ID))

        rmse = np.sqrt(mean_squared_error(true_patient.MTD.values, pred_patient.MTD.values))
        list_rmse.append(rmse)

        #         if extend_original:
        if true_extension is not None:
            true_extension = ext_groups.get_group(ID).iloc[0:-1, :]
            if not fine_sampling:
                pred_extension = pred_groups.get_group(ID).iloc[n_samples:, :]
            else:
                last_true_t = true_patient.t.values[-1]
                pred_extension = sample_by_nearest_time(true_extension, pred_groups.get_group(ID).loc[
                                                                        pred_groups.get_group(ID).t > last_true_t, :])

            list_true_extension.append(true_extension)
            list_pred_extension.append(pred_extension)

            rmse_ext = np.sqrt(mean_squared_error(true_extension.MTD.values, pred_extension.MTD.values))
            list_rmse_ext.append(rmse_ext)
    return list_rmse, list_rmse_ext, list_true_extension, list_pred_extension
