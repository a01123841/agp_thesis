import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from scipy.stats import lognorm
#plt.style.use('ggplot')


def fit_plot_lognormal(data, n_bins=21, title='Histogram and fit', xlabel = 'x', ylabel = 'Normalized count',
                       plot=True, negative=False, alpha=.75, color=None, line_color=None, ax=None, return_ax=False,
                       orientation='vertical', normal=False):
    """ Returns the pdf so sample can be drawn from it.
    Also mean and sigma

        title = 'Time final'
        data = last_time_df.loc[:,title]
        n_bins = 21

    Get current axis and scale it (to see it as a gaussian)
    plt.gca().set_xscale("log")

        pdf = (np.exp(-(np.log(x) - mu)**2 / (2 * sigma**2))
    ...        / (x * sigma * np.sqrt(2 * np.pi)))

    Args:
        data:
        n_bins:
        title:
        xlabel:
        ylabel:
        plot:
        negative:
        alpha:
        color:
        line_color:
        ax:
        return_ax:
        orientation:

    Returns:

    """


    if isinstance(data, (pd.Series, np.ndarray)):
        data = [data]

    if ax is None and plot:
        fig, ax = plt.subplots()

    counts = []
    bin_edges = []
    x_fit = []
    pdf = []
    mu = []
    sigma = []
    c = None
    for ii in np.arange(len(data)):
        if negative:
            # Fit
            shape, loc, scale = lognorm.fit(-1 * data[ii], floc=0)
            print('shape, loc, scale', shape, loc, scale)
            current_x_fit = np.linspace(min(data[ii]), max(data[ii]), 100)
            x_fit.append(current_x_fit)
            current_pdf = lognorm.pdf(-1 * current_x_fit, shape, loc, scale)
            pdf.append(current_pdf)
            scale = -1 * scale
        else:
            # Fit
            shape, loc, scale = lognorm.fit(data[ii], floc=0)
            current_x_fit = np.linspace(min(data[ii]), max(data[ii]), 100)
            x_fit.append(current_x_fit)
            current_pdf = lognorm.pdf(current_x_fit, shape, loc, scale)
            pdf.append(current_pdf)
        mu.append(scale)
        sigma.append(shape)
        if isinstance(line_color, (np.ndarray, list)):
            c = line_color[ii]
        else:
            c = line_color
        if plot:
            ax.plot(current_x_fit, current_pdf, label='Fit and area-scaled PDF', linewidth=2, c=c)
            if orientation == 'vertical':
                ax.axvline(x=scale, color='k', label='Mean')
            if orientation == 'horizontal':
                ax.axhline(y=scale, color='k', label='Mean')
            plt.legend()
            plt.title(title)

    # WRONG!
    #     mu = np.mean(pdf)
    #     sigma = np.std(pdf)

    #   print('scipy.stats: shape %f, loc %f, scale %f', shape, loc, scale)/
    #   print('numpy: mu, sigma', mu, sigma)

    # calculate area of histogram (area under PDF should be 1)
    # area_hist = .0
    # for ii in range(counts.size):
    #     area_hist += (bin_edges[ii+1]-bin_edges[ii]) * counts[ii]
    # oplot fit into histogram
    # plt.plot( x_fit, pdf*area_hist, label='fitted and area-scaled PDF', linewidth=2)
    if plot:
        current_count, current_bin_edges, _ = ax.hist(data, n_bins, normed=1, alpha=alpha, color=color, orientation=orientation)
        counts.append(current_count)
        bin_edges.append(current_bin_edges)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        plt.show()

    print('mu =, sigma =', mu, sigma)

    if return_ax:
        return [x_fit, pdf, mu, sigma, counts, bin_edges, ax]

    return [x_fit, pdf, mu, sigma, counts, bin_edges]

