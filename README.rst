==========================================================================================
agp_thesis: a library that uses LSTM rNNs for Parameter Inference in Sequential Modeling
==========================================================================================

Library created by Andr�s Gonz�lez Padilla (ga89qiy@mytum.de) for the Master Thesis titled **"LSTM rNNs for Parameter Inference in Sequential Modeling".**

The results were expanded and developed into an academic paper called **"Model-aware Recurrent Neural Network for prediction of tumor growth and response to treatment in Low-Grade Glioma: towards treatment personalization."** The paper was submitted to the BMC Systems Medicine journal.

The last version of the manuscript can be found in the root folder as a `pdf <https://bitbucket.org/a01123841/agp_thesis/raw/be665b86ea557659ade52e9ce90cb00e042ce0fe/bmc_article_submitted.pdf>`_.



Abstract
========
**Background:** The digitalization of clinical practice, through Electronic Health Records in particular, enables the application of powerful machine learning techniques for tasks such as diagnosis and treatment planning. In this work, we apply state-of-the-art Recurrent Neural Networks (RNN) to make personalized and interpretable predictions for tumor growth and response to treatment, from past observations.

**Method:** A tumor growth dynamical model, including treatment, is used to generate synthetic observations from virtual patients, statistically similar to clinical data. These data are used to train a Long short-term memory RNN to perform parameter inference of the tumor model.
Using transfer learning, the RNN is then applied to infer patients parameters for a smaller clinical dataset of Low-Grade Glioma, which are then used for forecasting.

**Results:** On synthetic data, our model-aware RNN performs better than baseline Maximum *a Posteriori* estimations, on parameter inference, regression of past observations, as well as future predictions. When transferred to clinical data, plagued with missing values, the model does not outperform the baseline approach in regression, but is comparable in the task of prediction and considerably faster.

**Conclusion:** Joining the forces of machine learning and model-based approaches, our method enables the encoding of complex dynamical systems in a RNN for individual parameter inference. This approach permits to alleviate problems of data scarcity and partially circumvents the black-box problem through the use of interpretable biophysical parameters. It shows promising results in transfer learning to patient collected data and can be extrapolated to other diseases in the context of precision medicine.




Description of the files found here:
====================================

All the necessary functions needed to create the Artificial Data and to build, optimize and test the Network can be found inside the src/agp_thesis directory. Please note that most functions are yet to be documented. Questions, commentaries and requests to use any part of this work should be addressed to the email address written above.
